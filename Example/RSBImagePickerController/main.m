//
//  main.m
//  RSBImagePickerController
//
//  Created by Anton Kormakov on 06/03/2016.
//  Copyright (c) 2016 Anton Kormakov. All rights reserved.
//

@import UIKit;
#import "RSBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RSBAppDelegate class]));
    }
}
