# RSBImagePickerController

[![CI Status](http://img.shields.io/travis/Anton Kormakov/RSBImagePickerController.svg?style=flat)](https://travis-ci.org/Anton Kormakov/RSBImagePickerController)
[![Version](https://img.shields.io/cocoapods/v/RSBImagePickerController.svg?style=flat)](http://cocoapods.org/pods/RSBImagePickerController)
[![License](https://img.shields.io/cocoapods/l/RSBImagePickerController.svg?style=flat)](http://cocoapods.org/pods/RSBImagePickerController)
[![Platform](https://img.shields.io/cocoapods/p/RSBImagePickerController.svg?style=flat)](http://cocoapods.org/pods/RSBImagePickerController)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RSBImagePickerController is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RSBImagePickerController"
```

## Author

Anton Kormakov, anton.kormakov@rosberry.com

## License

RSBImagePickerController is available under the MIT license. See the LICENSE file for more info.
