//
//  ImagePickerMaskView.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "RSBImagePickerMaskView.h"

@implementation RSBImagePickerMaskView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

#pragma mark - Setters/Getters

- (void)setMaskInsets:(UIEdgeInsets)maskInsets {
    
    _maskInsets = maskInsets;
    [self setNeedsDisplay];
}

- (void)setMaskColor:(UIColor *)maskColor {
    
    _maskColor = maskColor;
    [self setNeedsDisplay];
}

- (void)setBlendMode:(CGBlendMode)blendMode {
    
    _blendMode = blendMode;
    [self setNeedsDisplay];
}

#pragma mark - Draw

- (void)drawRect:(CGRect)rect {
    
    [self.maskColor setFill];
    UIRectFill(rect);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetBlendMode(context, self.blendMode);

    CGRect maskRect = (CGRect){
        .origin.x = CGRectGetMinX(rect) + self.maskInsets.left,
        .origin.y = CGRectGetMinX(rect) + self.maskInsets.top,
        .size.width = CGRectGetWidth(rect) - self.maskInsets.left - self.maskInsets.right,
        .size.height = CGRectGetHeight(rect) - self.maskInsets.top - self.maskInsets.bottom,
    };
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:maskRect];
    [path fill];
    
    CGContextSetBlendMode(context, kCGBlendModeNormal);
}

@end
