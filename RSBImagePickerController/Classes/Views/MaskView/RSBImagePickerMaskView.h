//
//  ImagePickerMaskView.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSBImagePickerMaskView : UIView

@property (nonatomic) UIEdgeInsets maskInsets;
@property (nonatomic) UIColor *maskColor;
@property (nonatomic) CGBlendMode blendMode;

@end
