//
//  ImagePickerPhotoViewAttributes.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 08/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ImagePickerPhotoViewAttributes <NSObject>

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) UIView *titleView;
@property (nonatomic, readonly) UIImage *image;
@property (nonatomic, readonly) CGRect cropRect;
@property (nonatomic, readonly) BOOL translucentTabBar;
@property (nonatomic, readonly) BOOL infoViewEnabled;
@property (nonatomic) CGFloat bottomInset;
@property (nonatomic) BOOL showMask;

- (void)fetchOriginalImageWithCompletionBlock:(void(^)(UIImage *image))completionBlock;

@end
