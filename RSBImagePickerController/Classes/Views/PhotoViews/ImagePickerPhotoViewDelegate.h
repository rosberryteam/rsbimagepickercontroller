//
//  ImagePickerPhotoViewDelegate.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 15/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoViewAttributes.h"

@protocol ImagePickerPhotoViewDelegate <NSObject>

@optional
- (void)imagePickerPhotoView:(UIView<ImagePickerPhotoViewAttributes> *)view infoViewEnabled:(BOOL)enabled;

@end
