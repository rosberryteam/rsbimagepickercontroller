//
//  ImagePickerPhotoCameraControlsView.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoCameraControlsView.h"
#import "ImagePickerPhotoCameraFlashButton.h"

#import "UIImage+RSBImagePickerBundle.h"

@interface ImagePickerPhotoCameraControlsView () <ImagePickerPhotoCameraFlashButtonDelegate>

@property (nonatomic) UIView *cameraControlsContainerView;
@property (nonatomic) UIButton *switchButton;
@property (nonatomic) UIButton *takePhotoButton;
@property (nonatomic) UIActivityIndicatorView *takePhotoActivityIndicatorView;
@property (nonatomic) ImagePickerPhotoCameraFlashButton *flashButton;

@property (nonatomic) UIView *previewControlsContainerView;
@property (nonatomic) UIButton *retakeButton;

@end

@implementation ImagePickerPhotoCameraControlsView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(orientationDidChangeNotification:)
                                                     name:UIDeviceOrientationDidChangeNotification
                                                   object:nil];
        
        self.cameraControlsContainerView = [[UIView alloc] init];
        [self addSubview:self.cameraControlsContainerView];
        
        self.switchButton = [[UIButton alloc] init];
        [self.switchButton addTarget:self action:@selector(switchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.switchButton setImage:[UIImage rsb_imagepicker_bundledImage:@"RSBImagePickerIconCameraRotate"] forState:UIControlStateNormal];
        [self.cameraControlsContainerView addSubview:self.switchButton];
        
        self.takePhotoButton = [[UIButton alloc] init];
        [self.takePhotoButton addTarget:self action:@selector(takePhotoButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.takePhotoButton setImage:[UIImage rsb_imagepicker_bundledImage:@"RSBImagePickerIconCameraShutter"] forState:UIControlStateNormal];
        [self.cameraControlsContainerView addSubview:self.takePhotoButton];
        
        self.takePhotoActivityIndicatorView = [[UIActivityIndicatorView alloc] init];
        self.takePhotoActivityIndicatorView.color = [UIColor whiteColor];
        [self.cameraControlsContainerView addSubview:self.takePhotoActivityIndicatorView];
        
        self.flashButton = [[ImagePickerPhotoCameraFlashButton alloc] init];
        self.flashButton.delegate = self;
        [self.cameraControlsContainerView addSubview:self.flashButton];
        
        self.previewControlsContainerView = [[UIView alloc] init];
        [self addSubview:self.previewControlsContainerView];
        
        self.retakeButton = [[UIButton alloc] init];
        [self.retakeButton addTarget:self action:@selector(retakeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.retakeButton setImage:[UIImage rsb_imagepicker_bundledImage:@"RSBImagePickerIconCameraRetake"] forState:UIControlStateNormal];
        [self.previewControlsContainerView addSubview:self.retakeButton];
        
        self.controlsState = ImagePickerPhotoCameraControlsStateCamera;
        [self rotateControlsWithOrientation:[UIDevice currentDevice].orientation];
    }
    return self;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.cameraControlsContainerView.frame = self.bounds;
    self.previewControlsContainerView.frame = self.bounds;
    
    self.switchButton.bounds = (CGRect){
        .size.width = 50.0,
        .size.height = 50.0,
    };
    self.switchButton.center = (CGPoint){
        .x = self.cameraControlsContainerView.bounds.size.width / 4.0,
        .y = self.cameraControlsContainerView.bounds.size.height / 2.0,
    };
    
    self.takePhotoButton.bounds = (CGRect){
        .size.width = self.cameraControlsContainerView.bounds.size.height - 10.0,
        .size.height = self.cameraControlsContainerView.bounds.size.height - 10.0,
    };
    self.takePhotoButton.center = (CGPoint){
        .x = self.cameraControlsContainerView.bounds.size.width / 2.0,
        .y = self.cameraControlsContainerView.bounds.size.height / 2.0,
    };
    
    [self.takePhotoActivityIndicatorView sizeToFit];
    self.takePhotoActivityIndicatorView.center = self.takePhotoButton.center;
    
    self.flashButton.bounds = self.switchButton.bounds;
    self.flashButton.center = (CGPoint){
        .x = 3.0 * self.cameraControlsContainerView.bounds.size.width / 4.0,
        .y = self.cameraControlsContainerView.bounds.size.height / 2.0,
    };
    
    self.retakeButton.bounds = (CGRect){
        .size.width = 50.0,
        .size.height = 50.0,
    };
    self.retakeButton.center = (CGPoint){
        .x = self.previewControlsContainerView.bounds.size.width / 2.0,
        .y = self.previewControlsContainerView.bounds.size.height / 2.0,
    };
}

#pragma mark - Setters/Getters

- (void)setControlsState:(ImagePickerPhotoCameraState)controlsState {
    
    _controlsState = controlsState;
    switch (_controlsState) {
        case ImagePickerPhotoCameraControlsStateCamera:
            self.cameraControlsContainerView.hidden = NO;
            self.previewControlsContainerView.hidden = YES;
            break;
        case ImagePickerPhotoCameraControlsStatePreview:
            self.cameraControlsContainerView.hidden = YES;
            self.previewControlsContainerView.hidden = NO;
            break;
    }
}

- (void)setFlashEnabled:(BOOL)flashEnabled {
    
    _flashEnabled = flashEnabled;
    self.flashButton.enabled = _flashEnabled;
}

#pragma mark - Actions

- (IBAction)switchButtonPressed:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoCameraControlsViewSwitchButtonPressed:)]) {
        [self.delegate imagePickerPhotoCameraControlsViewSwitchButtonPressed:self];
    }
}

- (IBAction)takePhotoButtonPressed:(id)sender {

    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoCameraControlsViewTakePhotoButtonPreessed:)]) {
        [self.delegate imagePickerPhotoCameraControlsViewTakePhotoButtonPreessed:self];
    }
}

- (IBAction)retakeButtonPressed:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoCameraControlsViewRetakeButtonPressed:)]) {
        [self.delegate imagePickerPhotoCameraControlsViewRetakeButtonPressed:self];
    }
}

#pragma mark - Views 

- (void)startAnimatingTakePhotoActivityIndicator {
    
    self.cameraControlsContainerView.userInteractionEnabled = NO;
    [self.takePhotoActivityIndicatorView startAnimating];
}

- (void)stopAnimatingTakePhotoActivityIndicator {
    
    self.cameraControlsContainerView.userInteractionEnabled = YES;
    [self.takePhotoActivityIndicatorView stopAnimating];
}

- (void)rotateControlsWithOrientation:(UIDeviceOrientation)orientation {
    
    CGFloat angle = 0.0;
    switch (orientation) {
        case UIDeviceOrientationLandscapeLeft:
            angle = M_PI / 2;
            break;
        case UIDeviceOrientationLandscapeRight:
            angle = -(M_PI / 2);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            angle = M_PI;
        default:
            break;
    }
    CGAffineTransform transform = CGAffineTransformMakeRotation(angle);
    [UIView animateWithDuration:0.3f animations:^{
        self.switchButton.transform = transform;
        self.takePhotoButton.transform = transform;
        self.flashButton.transform = transform;
    }];
}

#pragma mark - Protocols

#pragma mark ImagePickerPhotoCameraFlashButtonDelegate

- (void)imagePickerPhotoCameraFlashButton:(ImagePickerPhotoCameraFlashButton *)button
                        flashStateChanged:(ImagePickerPhotoCameraFlashMode)flashState {
    
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoCameraControlsView:flashStateChanged:)]) {
        [self.delegate imagePickerPhotoCameraControlsView:self flashStateChanged:flashState];
    }
}

#pragma mark - Notifications

- (void)orientationDidChangeNotification:(NSNotification *)notification {
    
    [self rotateControlsWithOrientation:[UIDevice currentDevice].orientation];
}

@end
