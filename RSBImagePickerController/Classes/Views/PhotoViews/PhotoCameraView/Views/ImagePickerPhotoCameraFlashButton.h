//
//  ImagePickerPhotoCameraFlashButton.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagePickerPhotoCameraConstants.h"

@class ImagePickerPhotoCameraFlashButton;

@protocol ImagePickerPhotoCameraFlashButtonDelegate <NSObject>

@optional
- (void)imagePickerPhotoCameraFlashButton:(ImagePickerPhotoCameraFlashButton *)button
                        flashStateChanged:(ImagePickerPhotoCameraFlashMode)flashState;

@end

@interface ImagePickerPhotoCameraFlashButton : UIControl

@property (nonatomic, weak) id<ImagePickerPhotoCameraFlashButtonDelegate> delegate;
@property (nonatomic) ImagePickerPhotoCameraFlashMode flashState;

@end
