//
//  ImagePickerPhotoCameraFlashButton.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoCameraFlashButton.h"

#import "UIImage+RSBImagePickerBundle.h"

@interface ImagePickerPhotoCameraFlashButton ()

@property (nonatomic) UIView *contentView;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *label;
@property (nonatomic) NSArray<NSNumber *> *flashStates;

@end

@implementation ImagePickerPhotoCameraFlashButton

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.contentView = [[UIView alloc] init];
        [self addSubview:self.contentView];
        
        self.label = [[UILabel alloc] init];
        self.label.textColor = [UIColor lightGrayColor];
        self.label.font = [UIFont systemFontOfSize:10.0];
        [self.contentView addSubview:self.label];
        
        self.imageView = [[UIImageView alloc] init];
        self.imageView.image = [UIImage rsb_imagepicker_bundledImage:@"RSBImagePickerIconCameraFlash"];
        [self.contentView addSubview:self.imageView];
        
        self.flashState = ImagePickerPhotoCameraFlashStateOff;
        self.flashStates = @[
                             @(ImagePickerPhotoCameraFlashStateOff),
                             @(ImagePickerPhotoCameraFlashStateAuto),
                             @(ImagePickerPhotoCameraFlashStateOn)
                             ];
        self.contentView.userInteractionEnabled = NO;
        [self addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.imageView sizeToFit];
    self.imageView.frame = self.imageView.bounds;
    
    [self.label sizeToFit];
    self.label.frame = (CGRect){
        .origin.x = CGRectGetMaxX(self.imageView.frame),
        .origin.y = self.imageView.bounds.size.height - self.label.bounds.size.height,
        .size = self.label.bounds.size,
    };
    
    self.contentView.bounds = (CGRect){
        .size.width = CGRectGetMaxX(self.label.frame),
        .size.height = self.imageView.bounds.size.height,
    };
    self.contentView.center = (CGPoint){
        .x = self.bounds.size.width / 2.0,
        .y = self.bounds.size.height / 2.0,
    };
}

#pragma mark - Setters/Getters

- (void)setHighlighted:(BOOL)highlighted {
    
    [super setHighlighted:highlighted];
    self.contentView.alpha = highlighted ? 0.5f : 1.0f;
}

- (void)setEnabled:(BOOL)enabled {
    
    [super setEnabled:enabled];
    self.userInteractionEnabled = enabled;
    self.contentView.alpha = enabled ? 1.0f : 0.3f;
}

- (void)setFlashState:(ImagePickerPhotoCameraFlashMode)flashState {
    
    _flashState = flashState;
    switch (flashState) {
        case ImagePickerPhotoCameraFlashStateOff: {
            self.label.text = NSLocalizedString(@"Off", nil);
            break;
        }
        case ImagePickerPhotoCameraFlashStateAuto: {
            self.label.text = NSLocalizedString(@"Auto", nil);
            break;
        }
        case ImagePickerPhotoCameraFlashStateOn: {
            self.label.text = NSLocalizedString(@"On", nil);
            break;
        }
    }
    
    [self setNeedsLayout];
}

#pragma mark - Actions

- (void)pressed:(id)sender {
    
    NSUInteger index = [self.flashStates indexOfObject:@(self.flashState)];
    index++;
    if (index == self.flashStates.count) {
        index = 0;
    }
    
    self.flashState = self.flashStates[index].integerValue;
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoCameraFlashButton:flashStateChanged:)]) {
        [self.delegate imagePickerPhotoCameraFlashButton:self flashStateChanged:self.flashState];
    }
}

@end
