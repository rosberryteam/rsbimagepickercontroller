//
//  ImagePickerPhotoCameraControlsView.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagePickerPhotoCameraConstants.h"

@class ImagePickerPhotoCameraControlsView;

@protocol ImagePickerPhotoCameraControlsViewDelegate <NSObject>

@optional
- (void)imagePickerPhotoCameraControlsViewSwitchButtonPressed:(ImagePickerPhotoCameraControlsView *)view;
- (void)imagePickerPhotoCameraControlsViewTakePhotoButtonPreessed:(ImagePickerPhotoCameraControlsView *)view;
- (void)imagePickerPhotoCameraControlsView:(ImagePickerPhotoCameraControlsView *)view
                         flashStateChanged:(ImagePickerPhotoCameraFlashMode)flashState;
- (void)imagePickerPhotoCameraControlsViewRetakeButtonPressed:(ImagePickerPhotoCameraControlsView *)view;

@end

@interface ImagePickerPhotoCameraControlsView : UIView

@property (nonatomic, weak) id<ImagePickerPhotoCameraControlsViewDelegate> delegate;
@property (nonatomic) ImagePickerPhotoCameraState controlsState;
@property (nonatomic) BOOL flashEnabled;

- (void)startAnimatingTakePhotoActivityIndicator;
- (void)stopAnimatingTakePhotoActivityIndicator;

@end
