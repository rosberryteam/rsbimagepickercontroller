//
//  PhotoCameraView.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@class RSBImagePickerPhotoCameraOutputView;

@protocol RSBImagePickerPhotoCameraOutputViewDelegate <NSObject>

@optional
- (void)photoCameraView:(RSBImagePickerPhotoCameraOutputView *)view flashEnabled:(BOOL)enabled;

@end


@interface RSBImagePickerPhotoCameraOutputView : UIView

@property (nonatomic, weak) id<RSBImagePickerPhotoCameraOutputViewDelegate> delegate;
@property (nonatomic) AVCaptureDevicePosition photoCameraPosition;
@property (nonatomic) AVCaptureFlashMode flashMode;

- (void)startSession;
- (void)takePhotoWithCompletionBlock:(void(^)(UIImage *, NSError *))completionBlock;
- (void)takeResizedPhotoWithMinDimension:(CGFloat)minDimension completionBlock:(void(^)(UIImage *, UIImage *, NSError *))completionBlock;
+ (void)requestAccessWithCompletionBlock:(void(^)(BOOL))completionBlock;

@end
