//
//  PhotoCameraView.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 08/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoCameraView.h"
#import "ImagePickerPhotoCameraControlsView.h"
#import "ImagePickerInfoView.h"
#import "RSBImagePickerMaskView.h"
#import "RSBImagePickerPhotoCameraOutputView.h"
#import "RSBImagePickerCropView.h"

@interface ImagePickerPhotoCameraView () <ImagePickerPhotoCameraControlsViewDelegate, RSBImagePickerCropViewDelegate, RSBImagePickerPhotoCameraOutputViewDelegate>

@property (nonatomic) RSBImagePickerPhotoCameraOutputView *cameraOutputView;
@property (nonatomic) RSBImagePickerCropView *cropView;
@property (nonatomic) RSBImagePickerMaskView *maskView;
@property (nonatomic) ImagePickerPhotoCameraControlsView *controlsView;
@property (nonatomic) ImagePickerInfoView *infoView;
@property (nonatomic) UILabel *titleLabel;

@property (nonatomic) ImagePickerPhotoCameraState state;
@property (nonatomic) UIImage *image;
@property (nonatomic) CGRect cropRect;
@property (nonatomic) BOOL infoViewEnabled;

@end

@implementation ImagePickerPhotoCameraView

@synthesize bottomInset = _bottomInset;
@synthesize showMask = _showMask;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        self.cropView = [[RSBImagePickerCropView alloc] init];
        self.cropView.delegate = self;
        self.cropView.backgroundColor = [UIColor blackColor];
        [self addSubview:self.cropView];
        
        self.cameraOutputView = [[RSBImagePickerPhotoCameraOutputView alloc] init];
        self.cameraOutputView.delegate = self;
        [self.cameraOutputView startSession];
        [self addSubview:self.cameraOutputView];
        
        self.maskView = [[RSBImagePickerMaskView alloc] init];
        self.maskView.maskInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        self.maskView.maskColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9f];
        self.maskView.blendMode = kCGBlendModeDestinationOut;
        self.maskView.userInteractionEnabled = NO;
        [self addSubview:self.maskView];
        
        self.controlsView = [[ImagePickerPhotoCameraControlsView alloc] init];
        self.controlsView.backgroundColor = [UIColor whiteColor];
        self.controlsView.delegate = self;
        [self addSubview:self.controlsView];
        
        self.infoView = [[ImagePickerInfoView alloc] init];
        self.infoView.title = NSLocalizedString(@"App does not have access to your camera.", nil);
        self.infoViewEnabled = YES;
        __weak typeof (self) weakSelf = self;
        [RSBImagePickerPhotoCameraOutputView requestAccessWithCompletionBlock:^(BOOL granted) {
            weakSelf.infoViewEnabled = !granted;
        }];
        [self addSubview:self.infoView];
        
        self.state = ImagePickerPhotoCameraControlsStateCamera;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.cameraOutputView.frame = (CGRect){
        .size.width = self.bounds.size.width,
        .size.height = self.bounds.size.width,
    };
    
    self.cropView.frame = self.cameraOutputView.frame;
    self.maskView.frame = self.cameraOutputView.frame;
    
    self.controlsView.frame = (CGRect){
        .origin.y = CGRectGetMaxY(self.cameraOutputView.frame),
        .size.width = self.bounds.size.width,
        .size.height = self.bounds.size.height - CGRectGetMaxY(self.cameraOutputView.frame) - self.bottomInset,
    };
    
    self.infoView.frame = self.bounds;
}

#pragma mark - Setters/Getters

- (void)setState:(ImagePickerPhotoCameraState)state {
    
    _state = state;
    
    switch (_state) {
        case ImagePickerPhotoCameraControlsStateCamera: {
            self.cameraOutputView.hidden = NO;
            self.cropView.hidden = YES;
            self.image = nil;
        }
            break;
        case ImagePickerPhotoCameraControlsStatePreview: {
            self.cameraOutputView.hidden = YES;
            self.cropView.hidden = NO;
        }
            break;
    }
    self.controlsView.controlsState = _state;
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoCameraView:stateChanged:)]) {
        [self.delegate imagePickerPhotoCameraView:self stateChanged:_state];
    }
}

- (void)setInfoViewEnabled:(BOOL)infoViewEnabled {
    
    _infoViewEnabled = infoViewEnabled;
    self.infoView.hidden = !_infoViewEnabled;
    self.titleView.hidden = _infoViewEnabled;
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoView:infoViewEnabled:)]) {
        [self.delegate imagePickerPhotoView:self infoViewEnabled:_infoViewEnabled];
    }
}

- (UILabel *)titleLabel {
    
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = self.title;
    }
    return _titleLabel;

}

#pragma mark - Protocols

#pragma mark PhotoViewAttributes

- (NSString *)title {
    
    return @"Camera";
}

- (UIView *)titleView {
    
    [self.titleLabel sizeToFit];
    return self.titleLabel;
}

- (BOOL)translucentTabBar {
    
    return NO;
}

- (void)setBottomInset:(CGFloat)bottomInset {
    
    _bottomInset = bottomInset;
    self.infoView.verticalContentOffset = -_bottomInset;
    
    [self setNeedsLayout];
}

- (CGFloat)bottomInset {
    
    return _bottomInset;
}

- (void)setShowMask:(BOOL)showMask {
    
    _showMask = showMask;
    self.maskView.hidden = !_showMask;
}

- (BOOL)showMask {
    
    return _showMask;
}

- (void)fetchOriginalImageWithCompletionBlock:(void (^)(UIImage *))completionBlock {
    
    if (completionBlock) {
        completionBlock(self.image);
    }
}

#pragma mark ImagePickerPhotoCameraControlsViewDelegate

- (void)imagePickerPhotoCameraControlsViewSwitchButtonPressed:(ImagePickerPhotoCameraControlsView *)view {
    
    AVCaptureDevicePosition newPosition = AVCaptureDevicePositionUnspecified;
    if (self.cameraOutputView.photoCameraPosition == AVCaptureDevicePositionBack) {
        newPosition = AVCaptureDevicePositionFront;
    }
    else if (self.cameraOutputView.photoCameraPosition == AVCaptureDevicePositionFront){
        newPosition = AVCaptureDevicePositionBack;
    }
    self.cameraOutputView.photoCameraPosition = newPosition;
}

- (void)imagePickerPhotoCameraControlsViewTakePhotoButtonPreessed:(ImagePickerPhotoCameraControlsView *)view {
    
#if TARGET_IPHONE_SIMULATOR
    self.image = [[UIImage alloc] init];
    self.state = ImagePickerPhotoCameraControlsStatePreview;
#else
    CGFloat minDimension = CGRectGetWidth(self.cropView.frame) * [UIScreen mainScreen].scale;
    [self.controlsView startAnimatingTakePhotoActivityIndicator];
    __weak typeof (self) weakSelf = self;
    [self.cameraOutputView takeResizedPhotoWithMinDimension:minDimension completionBlock:^(UIImage *originalImage, UIImage *resizedImage, NSError *error) {
        [weakSelf.controlsView stopAnimatingTakePhotoActivityIndicator];
        weakSelf.image = originalImage;
        weakSelf.cropView.image = resizedImage;
        weakSelf.state = ImagePickerPhotoCameraControlsStatePreview;
    }];
#endif
}

- (void)imagePickerPhotoCameraControlsView:(ImagePickerPhotoCameraControlsView *)view
                         flashStateChanged:(ImagePickerPhotoCameraFlashMode)flashState {
    
    AVCaptureFlashMode flashMode = AVCaptureFlashModeOff;
    switch (flashState) {
        case ImagePickerPhotoCameraFlashStateOff:   flashMode = AVCaptureFlashModeOff;  break;
        case ImagePickerPhotoCameraFlashStateAuto:  flashMode = AVCaptureFlashModeAuto; break;
        case ImagePickerPhotoCameraFlashStateOn:    flashMode = AVCaptureFlashModeOn;   break;
    }
    self.cameraOutputView.flashMode = flashMode;
}

- (void)imagePickerPhotoCameraControlsViewRetakeButtonPressed:(ImagePickerPhotoCameraControlsView *)view {
    
    self.state = ImagePickerPhotoCameraControlsStateCamera;
}

#pragma mark CropViewDelegate

- (void)cropView:(RSBImagePickerCropView *)view didChangeVisibleRect:(CGRect)rect {
    
    self.cropRect = view.cropRect;
}

#pragma mark PhotoCameraViewDelegate

- (void)photoCameraView:(RSBImagePickerPhotoCameraOutputView *)view flashEnabled:(BOOL)enabled {
    
    self.controlsView.flashEnabled = enabled;
}
@end
