//
//  PhotoCameraView.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "RSBImagePickerPhotoCameraOutputView.h"

@interface RSBImagePickerPhotoCameraOutputView ()

@property (nonatomic) UIView *contentView;

@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDevice *captureDevice;
@property (nonatomic) AVCaptureDeviceInput *deviceInput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;
@property (nonatomic) AVCaptureVideoPreviewLayer *previewLayer;

@end

@implementation RSBImagePickerPhotoCameraOutputView

- (instancetype)init {
    
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.contentView = [[UIView alloc] init];
    [self addSubview:self.contentView];
    
    _photoCameraPosition = AVCaptureDevicePositionBack;
}

- (void)dealloc {
    [self.session stopRunning];
}

#pragma mark - Layout

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.contentView.frame = self.bounds;
    self.previewLayer.frame = self.contentView.bounds;
}

#pragma mark - Setters/Getters

- (void)setPhotoCameraPosition:(AVCaptureDevicePosition)photoCameraPosition {
    
    if (_photoCameraPosition == photoCameraPosition) {
        return;
    }
    
    _photoCameraPosition = photoCameraPosition;
    
    self.captureDevice = [self deviceWithPosition:self.photoCameraPosition];
    
    NSError *error;
    AVCaptureDeviceInput *deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice error:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    [UIView animateWithDuration:0.15f delay:0.0f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.contentView.alpha = 0.0;
    } completion:^(BOOL finished) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [self.session beginConfiguration];
            [self.session removeInput:self.deviceInput];
            if ([self.session canAddInput:deviceInput]) {
                self.deviceInput = deviceInput;
            }
            [self.session addInput:self.deviceInput];
            [self.session commitConfiguration];
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.15f delay:0.1f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.contentView.alpha = 1.0;
                } completion:nil];
            });
        });
    }];
}

- (void)setFlashMode:(AVCaptureFlashMode)flashMode {
    
    if (!self.captureDevice.hasFlash) {
        return;
    }
    
    _flashMode = flashMode;
    
    NSError *error;
    [self.captureDevice lockForConfiguration:&error];
    if (error) {
        NSLog(@"%@", [error localizedDescription]);
    }
    else {
        [self.captureDevice setFlashMode:flashMode];
        [self.captureDevice unlockForConfiguration];
    }
}

- (void)setCaptureDevice:(AVCaptureDevice *)captureDevice {
    
    _captureDevice = captureDevice;
    if ([self.delegate respondsToSelector:@selector(photoCameraView:flashEnabled:)]) {
        [self.delegate photoCameraView:self flashEnabled:self.captureDevice.hasFlash];
    }
}

#pragma mark - Session

- (void)startSession {
    
#if TARGET_IPHONE_SIMULATOR
    return;
#endif
    
    self.session = [[AVCaptureSession alloc] init];
    self.session.sessionPreset = AVCaptureSessionPresetPhoto;

    NSError *error = nil;
    self.captureDevice = [self deviceWithPosition:self.photoCameraPosition];
    self.deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice error:&error];
    if (error) {
        NSLog(@"%@", error.localizedDescription);
    }
    if ([self.session canAddInput:self.deviceInput]) {
        [self.session addInput:self.deviceInput];
    }
    
    self.stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    if ([self.session canAddOutput:self.stillImageOutput]) {
        [self.stillImageOutput setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
        [self.session addOutput:self.stillImageOutput];
    }
    
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.contentView.layer addSublayer:self.previewLayer];
    [self.session startRunning];
}

- (AVCaptureDevice *)deviceWithPosition:(AVCaptureDevicePosition)position {
    
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    AVCaptureDevice *captureDevice = nil;
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            captureDevice = device;
            break;
        }
    }
    if (!captureDevice) {
        captureDevice = devices.firstObject;
    }
    return captureDevice;
}

- (void)captureStillImageWithCompletionBlock:(void(^)(UIImage *, NSError *))completionBlock {
    
    AVCaptureConnection *connection = [self.stillImageOutput connectionWithMediaType:AVMediaTypeVideo];
    __weak typeof (self) weakSelf = self;
    [self.stillImageOutput captureStillImageAsynchronouslyFromConnection:connection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        if (error) {
            if (completionBlock) {
                completionBlock(nil, error);
            }
        }
        else {
            NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
            UIImage *image = [[UIImage alloc] initWithData:imageData];
            UIImage *rotatedImage = [weakSelf rotatedImage:image];
            if (completionBlock) {
                completionBlock(rotatedImage, nil);
            }
        }
    }];
}

- (UIImage *)rotatedImage:(UIImage *)image {
    
    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    if (self.photoCameraPosition == AVCaptureDevicePositionFront) {
        if (deviceOrientation == UIDeviceOrientationLandscapeLeft){
            deviceOrientation = UIDeviceOrientationLandscapeRight;
        } else if (deviceOrientation == UIDeviceOrientationLandscapeRight){
            deviceOrientation = UIDeviceOrientationLandscapeLeft;
        }
    }
    UIImageOrientation rotationOrientation = [self imageOrientationWithDeviceOrientation:deviceOrientation];
    return [[UIImage alloc] initWithCGImage:image.CGImage
                                      scale:1.0
                                orientation:rotationOrientation];
}

- (UIImageOrientation)imageOrientationWithDeviceOrientation:(UIDeviceOrientation)deviceOrientation {
    
    UIImageOrientation imageOrientation;
    switch (deviceOrientation) {
        case UIDeviceOrientationPortraitUpsideDown: imageOrientation = UIImageOrientationLeft;  break;
        case UIDeviceOrientationLandscapeRight:     imageOrientation = UIImageOrientationDown;  break;
        case UIDeviceOrientationLandscapeLeft:      imageOrientation = UIImageOrientationUp;    break;
        default:                                    imageOrientation = UIImageOrientationRight; break;
    }
    return imageOrientation;
}

#pragma mark - Interface

- (void)takePhotoWithCompletionBlock:(void(^)(UIImage *, NSError *))completionBlock {
    
    self.previewLayer.connection.enabled = NO;
    __weak typeof (self) weakSelf = self;
    [self captureStillImageWithCompletionBlock:^(UIImage *image, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.previewLayer.connection.enabled = YES;
            if (completionBlock) {
                completionBlock(image, error);
            }
        });
    }];
}

- (void)takeResizedPhotoWithMinDimension:(CGFloat)minDimension completionBlock:(void(^)(UIImage *, UIImage *, NSError *))completionBlock {
    
    self.previewLayer.connection.enabled = NO;
    __weak typeof (self) weakSelf = self;
    [self captureStillImageWithCompletionBlock:^(UIImage *image, NSError *error) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            
            CGFloat aspectRatio = image.size.width / image.size.height;
            CGSize imageSize = (aspectRatio > 1) ? CGSizeMake(minDimension * aspectRatio, minDimension) : CGSizeMake(minDimension, minDimension/aspectRatio);
            UIGraphicsBeginImageContextWithOptions(imageSize, NO, 1.0);
            [image drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
            UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            dispatch_async(dispatch_get_main_queue(), ^{
                weakSelf.previewLayer.connection.enabled = YES;
                if (completionBlock) {
                    completionBlock(image, resizedImage, error);
                }
            });
        });
    }];
}

+ (void)requestAccessWithCompletionBlock:(void(^)(BOOL))completionBlock {
    
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (completionBlock) {
                completionBlock(granted);
            }
        });
    }];
}

@end
