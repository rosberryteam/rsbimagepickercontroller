//
//  PhotoCameraView.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 08/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagePickerPhotoViewAttributes.h"
#import "ImagePickerPhotoViewDelegate.h"
#import "ImagePickerPhotoCameraConstants.h"

@class ImagePickerPhotoCameraView;

@protocol ImagePickerPhotoCameraViewDelegate <ImagePickerPhotoViewDelegate>

@optional
- (void)imagePickerPhotoCameraView:(ImagePickerPhotoCameraView *)view stateChanged:(ImagePickerPhotoCameraState)state;

@end

@interface ImagePickerPhotoCameraView : UIView <ImagePickerPhotoViewAttributes>

@property (nonatomic) id<ImagePickerPhotoCameraViewDelegate> delegate;
@property (nonatomic, readonly) ImagePickerPhotoCameraState state;

@end
