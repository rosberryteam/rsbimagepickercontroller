//
//  ImagePickerPhotoCameraConstants.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

typedef NS_ENUM(NSUInteger, ImagePickerPhotoCameraState) {
    ImagePickerPhotoCameraControlsStateCamera,
    ImagePickerPhotoCameraControlsStatePreview,
};

typedef NS_ENUM(NSUInteger, ImagePickerPhotoCameraFlashMode) {
    ImagePickerPhotoCameraFlashStateOff,
    ImagePickerPhotoCameraFlashStateAuto,
    ImagePickerPhotoCameraFlashStateOn,
};

