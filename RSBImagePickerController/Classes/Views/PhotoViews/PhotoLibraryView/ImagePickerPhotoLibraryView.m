//
//  PhotoLibraryView.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 08/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoLibraryView.h"
#import "ImagePickerPhotoLibraryAlbumContentsView.h"
#import "ImagePickerInfoView.h"
#import "RSBImagePickerDropDownButton.h"
#import "RSBImagePickerMaskView.h"
#import "RSBImagePickerCropView.h"

#import "PHAsset+RSBImagePickerAdditions.h"
#import "UIImage+RSBImagePickerResize.h"

#import <Photos/Photos.h>

typedef NS_ENUM(NSUInteger, AlbumContentsViewState) {
    AlbumContentsViewStateClosed,
    AlbumContentsViewStateOpened,
    AlbumContentsViewStateOpening,
    AlbumContentsViewStateOpeningWithAnimation,
    AlbumContentsViewStateClosing,
    AlbumContentsViewStateClosingWithAnimation,
};

@interface ImagePickerPhotoLibraryView ()
<
RSBImagePickerDropDownButtonDelegate,
RSBImagePickerCropViewDelegate,
ImagePickerPhotoLibraryAlbumContentsViewDelegate
>

@property (nonatomic) RSBImagePickerDropDownButton *titleDropDownButton;
@property (nonatomic) ImagePickerPhotoLibraryAlbumContentsView *albumContentsView;
@property (nonatomic) UIView *cropContainerView;
@property (nonatomic) RSBImagePickerCropView *cropView;
@property (nonatomic) RSBImagePickerMaskView *maskView;
@property (nonatomic) ImagePickerInfoView *infoView;

@property (nonatomic) CGFloat cropContainerViewOffset;

@property (nonatomic) CGRect cropRect;
@property (nonatomic) BOOL infoViewEnabled;
@property (nonatomic) PHAsset *asset;
@property (nonatomic) UIImage *image;

@property (nonatomic) AlbumContentsViewState albumContentsViewState;

@end

@implementation ImagePickerPhotoLibraryView

@synthesize assetCollection = _assetCollection;
@synthesize bottomInset = _bottomInset;
@synthesize showMask = _showMask;

- (id)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        self.albumContentsViewState = AlbumContentsViewStateClosed;
        
        self.albumContentsView = [[ImagePickerPhotoLibraryAlbumContentsView alloc] init];
        self.albumContentsView.delegate = self;
        [self addSubview:self.albumContentsView];
        
        self.cropContainerView = [[UIView alloc] init];
        [self addSubview:self.cropContainerView];
        
        self.cropView = [[RSBImagePickerCropView alloc] init];
        self.cropView.delegate = self;
        self.cropView.backgroundColor = [UIColor blackColor];
        [self.cropContainerView addSubview:self.cropView];
        
        self.maskView = [[RSBImagePickerMaskView alloc] init];
        self.maskView.maskInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        self.maskView.maskColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9f];
        self.maskView.blendMode = kCGBlendModeDestinationOut;
        self.maskView.userInteractionEnabled = NO;
        [self.cropContainerView addSubview:self.maskView];
        
        self.infoView = [[ImagePickerInfoView alloc] init];
        self.infoView.title = NSLocalizedString(@"App does not have access to your photo gallery.", nil);
        [self addSubview:self.infoView];
        
        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
        if (status == PHAuthorizationStatusAuthorized) {
            PHFetchResult *result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                             subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary
                                                                             options:nil];
            self.infoViewEnabled = NO;
            self.assetCollection = result.firstObject;
        }
        else {
            self.infoViewEnabled = YES;
            __weak typeof (self) weakSelf = self;
            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    PHFetchResult *result = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                                     subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary
                                                                                     options:nil];
                    weakSelf.infoViewEnabled = (status != PHAuthorizationStatusAuthorized);
                    weakSelf.assetCollection = result.firstObject;
                });
            }];
        }
    }
    
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.albumContentsView.frame = self.bounds;
    self.infoView.frame = self.bounds;
    
    self.cropContainerView.frame = (CGRect){
        .origin.y = self.cropContainerViewOffset,
        .size.width = self.bounds.size.width,
        .size.height = self.bounds.size.width,
    };
    
    self.cropView.frame = self.cropContainerView.bounds;
    self.maskView.frame = self.cropContainerView.bounds;
    
    UIEdgeInsets insets = self.albumContentsView.insets;
    insets.top = CGRectGetHeight(self.cropContainerView.frame);
    self.albumContentsView.insets = insets;
}

#pragma mark - Setters/Getters

- (void)setAssetCollection:(PHAssetCollection *)assetCollection {
    
    _assetCollection = assetCollection;
    
    self.albumContentsView.assetCollection = _assetCollection;
    self.titleDropDownButton.label.text = assetCollection.localizedTitle;
    self.titleDropDownButton.selected = NO;
    [self.titleDropDownButton sizeToFit];
 
    self.albumContentsViewState = AlbumContentsViewStateClosed;
}

- (void)setAsset:(PHAsset *)asset {
    
    _asset = asset;
    
    self.albumContentsViewState = AlbumContentsViewStateClosingWithAnimation;
    
    CGFloat imageWidth = [UIScreen mainScreen].bounds.size.width * [UIScreen mainScreen].scale;
    __weak typeof (self) weakSelf = self;
    self.image = nil;
    [asset rsb_imagepicker_requestImageWithSize:CGSizeMake(imageWidth, imageWidth) contentMode:PHImageContentModeAspectFill resultHandler:^(UIImage *resultImage) {
        weakSelf.image = resultImage;
        weakSelf.cropView.image = resultImage;
    }];
}

- (void)setInfoViewEnabled:(BOOL)infoViewEnabled {
    
    _infoViewEnabled = infoViewEnabled;
    self.infoView.hidden = !_infoViewEnabled;
    self.titleView.hidden = _infoViewEnabled;
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoView:infoViewEnabled:)]) {
        [self.delegate imagePickerPhotoView:self infoViewEnabled:_infoViewEnabled];
    }
}

- (void)setAlbumContentsViewState:(AlbumContentsViewState)albumContentsViewState {
    
    _albumContentsViewState = albumContentsViewState;
    
    switch (_albumContentsViewState) {
            
        case AlbumContentsViewStateOpened: {
            self.cropContainerViewOffset = -CGRectGetHeight(self.cropContainerView.frame);
            CGPoint contentOffset = self.albumContentsView.scrollView.contentOffset;
            contentOffset.y = 0;
            self.albumContentsView.scrollView.contentOffset = contentOffset;
        }
            break;
            
        case AlbumContentsViewStateOpeningWithAnimation: {
            self.cropContainerViewOffset = -CGRectGetHeight(self.cropContainerView.frame);
            [self setNeedsLayout];
            [UIView animateWithDuration:0.1f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
                [self layoutIfNeeded];
                CGPoint contentOffset = self.albumContentsView.scrollView.contentOffset;
                if (contentOffset.y < 0) {
                    contentOffset.y = 0;
                    self.albumContentsView.scrollView.contentOffset = contentOffset;
                }
            } completion:^(BOOL finished) {
                _albumContentsViewState = AlbumContentsViewStateOpened;
            }];
        }
            break;
            
        case AlbumContentsViewStateOpening: {
            CGPoint location = [self.albumContentsView.scrollView.panGestureRecognizer locationInView:self];
            CGFloat offset  = CGRectGetHeight(self.cropContainerView.frame) - location.y;
            if (offset >= CGRectGetHeight(self.cropContainerView.frame)) {
                offset = CGRectGetHeight(self.cropContainerView.frame);
            }
            self.cropContainerViewOffset = -offset;
        }
            break;

        case AlbumContentsViewStateClosed: {
            CGPoint contentOffset = self.albumContentsView.scrollView.contentOffset;
            contentOffset.y = -CGRectGetHeight(self.cropContainerView.frame);
            self.albumContentsView.scrollView.contentOffset = contentOffset;
            self.cropContainerViewOffset = 0;
        }
            break;

        case AlbumContentsViewStateClosing: {
            CGFloat offset = - self.albumContentsView.scrollView.contentOffset.y - CGRectGetHeight(self.cropContainerView.frame);
            if (offset > 0) {
                offset = 0;
                _albumContentsViewState = AlbumContentsViewStateClosed;
            }
            self.cropContainerViewOffset = offset;
        }
            break;
            
        case AlbumContentsViewStateClosingWithAnimation: {
            self.cropContainerViewOffset = 0;
            
            [self setNeedsLayout];
            [UIView animateWithDuration:0.1f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
                [self layoutIfNeeded];
            } completion:^(BOOL finished) {
                _albumContentsViewState = AlbumContentsViewStateClosed;
            }];
        }
            break;
            
        default:
            break;
    }
    
    [self setNeedsLayout];
}

- (RSBImagePickerDropDownButton *)titleDropDownButton {
    
    if (!_titleDropDownButton) {
        _titleDropDownButton = [[RSBImagePickerDropDownButton alloc] init];
        _titleDropDownButton.delegate = self;
        _titleDropDownButton.label.text = self.assetCollection.localizedTitle;
        _titleDropDownButton.label.textColor = [UIColor darkGrayColor];
        _titleDropDownButton.label.font = [UIFont systemFontOfSize:16.0];
    }
    return _titleDropDownButton;
}

#pragma mark - Protocols

#pragma mark ImagePickerPhotoViewAttributes

- (NSString *)title {
    
    return @"Library";
}

- (UIView *)titleView {
    
    [self.titleDropDownButton sizeToFit];
    return self.titleDropDownButton;
}

- (BOOL)translucentTabBar {
    
    return YES;
}

- (void)setBottomInset:(CGFloat)bottomInset {
 
    _bottomInset = bottomInset;
    self.infoView.verticalContentOffset = -_bottomInset;

    UIEdgeInsets insets = self.albumContentsView.insets;
    insets.bottom = bottomInset;
    self.albumContentsView.insets = insets;
    
    [self setNeedsLayout];
}

- (CGFloat)bottomInset {
    
    return _bottomInset;
}

- (void)fetchOriginalImageWithCompletionBlock:(void (^)(UIImage *))completionBlock {
    
    __weak typeof (self) weakSelf = self;
    [self.asset rsb_imagepicker_requestImageWithSize:PHImageManagerMaximumSize contentMode:PHImageContentModeAspectFill resultHandler:^(UIImage *resultImage) {
        if (completionBlock) {
            completionBlock(resultImage);
        }
    }];
}

#pragma mark TRDDropDownButtonDelegate

- (void)dropDownButtonPressed:(RSBImagePickerDropDownButton *)button {
    
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoLibraryView:dropDownButtonDidSelect:)]) {
        [self.delegate imagePickerPhotoLibraryView:self dropDownButtonDidSelect:button.selected];
    }
}

- (void)setShowMask:(BOOL)showMask {
    
    _showMask = showMask;
    self.maskView.hidden = !_showMask;
}

- (BOOL)showMask {
    
    return _showMask;
}

#pragma mark ImagePickerPhotoLibraryAlbumContentsViewDelegate

- (void)imagePickerPhotoLibraryAlbumContentsView:(ImagePickerPhotoLibraryAlbumContentsView *)view
                                  didSelectAsset:(PHAsset *)asset {
    
    self.asset = asset;
}

- (void)imagePickerPhotoLibraryAlbumContentsView:(ImagePickerPhotoLibraryAlbumContentsView *)view
                                  didEndDragging:(UIScrollView *)scrollView
                                  willDecelerate:(BOOL)decelerate {
    
    if (self.albumContentsViewState == AlbumContentsViewStateOpening) {
        CGPoint velocity = [scrollView.panGestureRecognizer velocityInView:self];
        if (velocity.y > 0) {
            self.albumContentsViewState = AlbumContentsViewStateClosing;
            return;
        }
        CGPoint location = [self.albumContentsView.scrollView.panGestureRecognizer locationInView:self];
        if (location.y > 0.0f) {
            self.albumContentsViewState = AlbumContentsViewStateOpeningWithAnimation;
        }
        else {
            self.albumContentsViewState = AlbumContentsViewStateOpened;
        }
    }
    else if (self.albumContentsViewState == AlbumContentsViewStateClosing && !decelerate) {
        self.albumContentsViewState = AlbumContentsViewStateClosing;
    }
}

- (void)imagePickerPhotoLibraryAlbumContentsView:(ImagePickerPhotoLibraryAlbumContentsView *)view
                                       didScroll:(UIScrollView *)scrollView {
    
    if (self.albumContentsView.scrollView.contentSize.height && (self.albumContentsView.scrollView.contentSize.height <= self.albumContentsView.scrollView.bounds.size.height)) {
        self.albumContentsViewState = AlbumContentsViewStateClosing;
        return;
    }
    
    CGPoint location = [self.albumContentsView.scrollView.panGestureRecognizer locationInView:self];
    CGFloat offset  = CGRectGetHeight(self.cropContainerView.frame) - location.y;
    if ((self.albumContentsViewState == AlbumContentsViewStateClosed || self.albumContentsViewState == AlbumContentsViewStateOpening) &&
        self.albumContentsView.scrollView.isDragging && !self.albumContentsView.scrollView.isDecelerating &&
        offset > 0 ) {
        self.albumContentsViewState = AlbumContentsViewStateOpening;
    }
    else if ((self.albumContentsViewState == AlbumContentsViewStateOpened || self.albumContentsViewState == AlbumContentsViewStateClosing) &&
             (self.albumContentsView.scrollView.isDragging || self.albumContentsView.scrollView.isDecelerating) &&
             self.albumContentsView.scrollView.contentOffset.y < 0) {
        self.albumContentsViewState = AlbumContentsViewStateClosing;
    }
}

#pragma mark CropViewDelegate

- (void)cropView:(RSBImagePickerCropView *)view didChangeVisibleRect:(CGRect)rect {
    
    self.cropRect = view.cropRect;
}

@end
