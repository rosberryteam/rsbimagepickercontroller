//
//  PhotoLibraryView.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 08/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagePickerPhotoViewAttributes.h"
#import "ImagePickerPhotoViewDelegate.h"

@class ImagePickerPhotoLibraryView;

@protocol ImagePickerPhotoLibraryViewDelegate <ImagePickerPhotoViewDelegate>

@optional
- (void)imagePickerPhotoLibraryView:(ImagePickerPhotoLibraryView *)view dropDownButtonDidSelect:(BOOL)selected;

@end

@class PHAssetCollection;

@interface ImagePickerPhotoLibraryView : UIView <ImagePickerPhotoViewAttributes>

@property (nonatomic, weak) id<ImagePickerPhotoLibraryViewDelegate> delegate;
@property (nonatomic) PHAssetCollection *assetCollection;

@end
