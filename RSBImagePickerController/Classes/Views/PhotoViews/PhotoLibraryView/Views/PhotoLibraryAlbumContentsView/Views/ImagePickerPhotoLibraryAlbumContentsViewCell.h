//
//  ImagePickerPhotoLibraryAlbumContentsViewCell.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 15/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerPhotoLibraryAlbumContentsViewCell : UICollectionViewCell

@property (nonatomic) UIImage *image;

@end
