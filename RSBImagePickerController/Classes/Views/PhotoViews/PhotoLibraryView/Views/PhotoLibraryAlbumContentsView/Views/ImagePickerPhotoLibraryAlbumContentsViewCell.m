//
//  ImagePickerPhotoLibraryAlbumContentsViewCell.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 15/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoLibraryAlbumContentsViewCell.h"

@interface ImagePickerPhotoLibraryAlbumContentsViewCell ()

@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UIView *borderedView;

@end

@implementation ImagePickerPhotoLibraryAlbumContentsViewCell

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.imageView = [[UIImageView alloc] init];
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        [self addSubview:self.imageView];
        
        self.borderedView = [[UIView alloc] init];
        self.borderedView.layer.borderWidth = 3.0f;
        self.borderedView.layer.borderColor = [UIColor colorWithRed:(0.0f/255.0f) green:(175.0f/255.0f) blue:(220.0f/255.0f) alpha:1.0f].CGColor;
        self.borderedView.hidden = YES;
        [self addSubview:self.borderedView];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.imageView.frame = self.bounds;
    self.borderedView.frame = self.bounds;
}

- (void)prepareForReuse {
    
    [super prepareForReuse];
    self.borderedView.hidden = YES;
}

- (void)setSelected:(BOOL)selected {
    
    [super setSelected:selected];
    self.borderedView.hidden = !selected;
}

#pragma mark - Setters/Getters

- (void)setImage:(UIImage *)image {
    
    self.imageView.image = image;
}

- (UIImage *)image {
    
    return self.imageView.image;
}

@end
