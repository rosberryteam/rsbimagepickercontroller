//
//  ImagePickerPhotoLibraryAlbumContentsView.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 12/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoLibraryAlbumContentsView.h"
#import "ImagePickerPhotoLibraryAlbumContentsViewCell.h"

#import "PHAsset+RSBImagePickerAdditions.h"

#import <Photos/Photos.h>

@interface ImagePickerPhotoLibraryAlbumContentsView ()
<
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout,
UIScrollViewDelegate,
PHPhotoLibraryChangeObserver
>

@property (nonatomic) UICollectionView *collectionView;

@property (nonatomic) PHFetchResult *fetchResult;

@property (nonatomic) BOOL performingCollectionViewUpdates;
@property (nonatomic, readonly) CGFloat interItemSpacing;
@property (nonatomic, readonly) NSUInteger itemsInRowCount;

@end

@implementation ImagePickerPhotoLibraryAlbumContentsView

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:(CGRect){} collectionViewLayout:[[UICollectionViewFlowLayout alloc] init]];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.alwaysBounceVertical = YES;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView registerClass:[ImagePickerPhotoLibraryAlbumContentsViewCell class]
          forCellWithReuseIdentifier:NSStringFromClass([ImagePickerPhotoLibraryAlbumContentsViewCell class])];
    [self addSubview:self.collectionView];
    
    self.performingCollectionViewUpdates = NO;    
    [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.collectionView.frame = self.bounds;
}

#pragma mark - Setters/Getters

- (void)setAssetCollection:(PHAssetCollection *)assetCollection {
    
    _assetCollection = assetCollection;
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %i", PHAssetMediaTypeImage];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:NO]];
    self.fetchResult = [PHAsset fetchAssetsInAssetCollection:_assetCollection options:fetchOptions];
    
    [self.collectionView reloadData];
    
    NSIndexPath *selectedIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    [self.collectionView selectItemAtIndexPath:selectedIndexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoLibraryAlbumContentsView:didSelectAsset:)]) {
        if (self.fetchResult.count > selectedIndexPath.item) {
            [self.delegate imagePickerPhotoLibraryAlbumContentsView:self didSelectAsset:self.fetchResult[selectedIndexPath.item]];
        }
    }
}

- (void)setInsets:(UIEdgeInsets)insets {
    
    self.collectionView.contentInset = insets;
}

- (UIEdgeInsets)insets {
    
    return self.collectionView.contentInset;
}

- (CGFloat)interItemSpacing {
    
    return 4.0f;
}

- (NSUInteger)itemsInRowCount {
    
    return 4;
}

- (UIScrollView *)scrollView {
    
    return self.collectionView;
}

#pragma mark - Helpers

- (NSArray *)itemsIndexPathsFromAssetsIndexes:(NSIndexSet*)indexes {
    
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:indexes.count];
    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [indexPaths addObject:[NSIndexPath indexPathForItem:idx+1 inSection:0]];
    }];
    return indexPaths;
}

#pragma mark - Protocols

#pragma mark UICollectionViewDatasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return self.fetchResult.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    PHAsset *asset = self.fetchResult[indexPath.item];
    
    ImagePickerPhotoLibraryAlbumContentsViewCell *cell =
    [cv dequeueReusableCellWithReuseIdentifier:NSStringFromClass([ImagePickerPhotoLibraryAlbumContentsViewCell class])
                                  forIndexPath:indexPath];
    
    PHImageRequestOptions *options = [PHImageRequestOptions new];
    options.resizeMode = PHImageRequestOptionsResizeModeFast;
    options.deliveryMode = PHImageRequestOptionsDeliveryModeOpportunistic;
    options.version = PHImageRequestOptionsVersionCurrent;
    options.synchronous = NO;
    
    CGSize requestImageSize = CGSizeMake(120, 120);
    [[PHCachingImageManager defaultManager] requestImageForAsset:asset
                                                      targetSize:requestImageSize
                                                     contentMode:PHImageContentModeAspectFit
                                                         options:options
                                                   resultHandler:^(UIImage *result, NSDictionary *info) {
                                                       cell.image = result;
                                                   }];

    return cell;

}

#pragma mark UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoLibraryAlbumContentsView:didSelectAsset:)]) {
        [self.delegate imagePickerPhotoLibraryAlbumContentsView:self didSelectAsset:self.fetchResult[indexPath.item]];
    }
}

#pragma mark UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UIEdgeInsets insets = [self collectionView:collectionView layout:collectionViewLayout insetForSectionAtIndex:indexPath.section];
    CGFloat width = (CGRectGetWidth(collectionView.frame) - insets.left - insets.right - (self.itemsInRowCount - 1) * self.interItemSpacing) / self.itemsInRowCount;
    return CGSizeMake(width, width);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout*)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(self.interItemSpacing, self.interItemSpacing, self.interItemSpacing, self.interItemSpacing);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return self.interItemSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout*)collectionViewLayout
minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return self.interItemSpacing;
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoLibraryAlbumContentsView:willBeginDragging:)]){
        [self.delegate imagePickerPhotoLibraryAlbumContentsView:self willBeginDragging:self.collectionView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoLibraryAlbumContentsView:didEndDragging:willDecelerate:)]){
        [self.delegate imagePickerPhotoLibraryAlbumContentsView:self didEndDragging:self.collectionView willDecelerate:decelerate];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoLibraryAlbumContentsView:didEndDecelerating:)]){
        [self.delegate imagePickerPhotoLibraryAlbumContentsView:self didEndDecelerating:self.collectionView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoLibraryAlbumContentsView:didScroll:)]){
        [self.delegate imagePickerPhotoLibraryAlbumContentsView:self didScroll:self.collectionView];
    }
}

#pragma mark PHPhotoLibraryChangeObserver

- (void)photoLibraryDidChange:(PHChange *)changeInstance {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        PHFetchResultChangeDetails *collectionChanges = [changeInstance changeDetailsForFetchResult:self.fetchResult];
        if (collectionChanges) {
            self.fetchResult = [collectionChanges fetchResultAfterChanges];
            if (![collectionChanges hasIncrementalChanges] || [collectionChanges hasMoves]){
                [self.collectionView reloadData];
            }
            else{
                if (!self.performingCollectionViewUpdates){
                    self.performingCollectionViewUpdates = YES;
                    [self.collectionView performBatchUpdates:^{
                        NSIndexSet *removedIndexes = [collectionChanges removedIndexes];
                        if ([removedIndexes count]) {
                            [self.collectionView deleteItemsAtIndexPaths:[self itemsIndexPathsFromAssetsIndexes:removedIndexes]];
                        }
                        NSIndexSet *insertedIndexes = [collectionChanges insertedIndexes];
                        if ([insertedIndexes count]) {
                            [self.collectionView insertItemsAtIndexPaths:[self itemsIndexPathsFromAssetsIndexes:insertedIndexes]];
                        }
                        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
                    } completion:^(BOOL finished) {
                        self.performingCollectionViewUpdates = NO;
                    }];
                }
            }
        }
    });
}

@end
