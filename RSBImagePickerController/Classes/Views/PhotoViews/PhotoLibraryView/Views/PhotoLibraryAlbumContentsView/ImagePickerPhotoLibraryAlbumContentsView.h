//
//  ImagePickerPhotoLibraryAlbumContentsView.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 12/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImagePickerPhotoLibraryAlbumContentsView;
@class PHAsset;

@protocol ImagePickerPhotoLibraryAlbumContentsViewDelegate <NSObject>

@optional
- (void)imagePickerPhotoLibraryAlbumContentsView:(ImagePickerPhotoLibraryAlbumContentsView *)view
                                  didSelectAsset:(PHAsset *)asset;
- (void)imagePickerPhotoLibraryAlbumContentsView:(ImagePickerPhotoLibraryAlbumContentsView *)view
                               willBeginDragging:(UIScrollView *)scrollView;
- (void)imagePickerPhotoLibraryAlbumContentsView:(ImagePickerPhotoLibraryAlbumContentsView *)view
                                  didEndDragging:(UIScrollView *)scrollView
                                  willDecelerate:(BOOL)decelerate;
- (void)imagePickerPhotoLibraryAlbumContentsView:(ImagePickerPhotoLibraryAlbumContentsView *)view
                              didEndDecelerating:(UIScrollView *)scrollView;
- (void)imagePickerPhotoLibraryAlbumContentsView:(ImagePickerPhotoLibraryAlbumContentsView *)view
                                             didScroll:(UIScrollView *)scrollView;

@end

@class PHAssetCollection;

@interface ImagePickerPhotoLibraryAlbumContentsView : UIView

@property (nonatomic, weak) id<ImagePickerPhotoLibraryAlbumContentsViewDelegate> delegate;
@property (nonatomic) PHAssetCollection *assetCollection;
@property (nonatomic) UIEdgeInsets insets;
@property (nonatomic, readonly) UIScrollView *scrollView;

@end
