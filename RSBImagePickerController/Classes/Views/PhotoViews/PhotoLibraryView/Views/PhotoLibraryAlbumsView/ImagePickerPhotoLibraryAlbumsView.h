//
//  ImagePickerPhotoLibraryAlbumsView.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 12/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImagePickerPhotoLibraryAlbumsView;
@class PHAssetCollection;

@protocol ImagePickerPhotoLibraryAlbumsViewDelegate <NSObject>

@optional
- (void)imagePickerPhotoLibraryAlbumsView:(ImagePickerPhotoLibraryAlbumsView *)view
                 didSelectAssetCollection:(PHAssetCollection *)assetCollection;

@end

@interface ImagePickerPhotoLibraryAlbumsView : UIView

@property (nonatomic, assign) UIEdgeInsets insets;
@property (nonatomic, weak) id<ImagePickerPhotoLibraryAlbumsViewDelegate> delegate;

@end
