//
//  ImagePickerPhotoLibraryAlbumsViewCell.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 12/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoLibraryAlbumsViewCell.h"

@implementation ImagePickerPhotoLibraryAlbumsViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier]) {
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        self.imageView.clipsToBounds = YES;
        self.tag = -1;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    if (!self.imageView.superview) {
        [self.contentView addSubview:self.imageView];
    }
    self.imageView.frame = (CGRect){
        .origin.x = 8.0,
        .origin.y = 8.0,
        .size.width = self.contentView.bounds.size.height - 16.0,
        .size.height = self.contentView.bounds.size.height - 16.0,
    };
    self.imageView.layer.cornerRadius = self.imageView.bounds.size.width / 2.0;
    
    self.textLabel.frame = (CGRect){
        .origin.x = CGRectGetMaxX(self.imageView.frame) + 8.0,
        .origin.y = self.textLabel.frame.origin.y,
        .size = self.textLabel.bounds.size,
    };
    
    self.detailTextLabel.frame = (CGRect){
        .origin.x = CGRectGetMaxX(self.imageView.frame) + 8.0,
        .origin.y = self.detailTextLabel.frame.origin.y,
        .size = self.detailTextLabel.bounds.size,
    };
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    
    [super setHighlighted:highlighted animated:animated];
    self.alpha = highlighted ? 0.5f : 1.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];
    self.alpha = selected ? 0.5f : 1.0f;
}

@end
