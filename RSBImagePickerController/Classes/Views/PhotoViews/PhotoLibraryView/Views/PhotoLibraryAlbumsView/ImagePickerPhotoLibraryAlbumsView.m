//
//  ImagePickerPhotoLibraryAlbumsView.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 12/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoLibraryAlbumsView.h"
#import "ImagePickerPhotoLibraryAlbumsViewCell.h"

#import "PHAsset+RSBImagePickerAdditions.h"
#import "PHAssetCollection+RSBImagePickerAdditions.h"

#import <Photos/Photos.h>

@interface ImagePickerPhotoLibraryAlbumsView () <UITableViewDataSource, UITableViewDelegate, PHPhotoLibraryChangeObserver>

@property (nonatomic) UITableView *tableView;
@property (nonatomic) NSLayoutConstraint *topConstraint;

@property (nonatomic) PHAuthorizationStatus authorizationStatus;
@property (nonatomic) NSArray<PHAssetCollection *> *albums;

@end

@implementation ImagePickerPhotoLibraryAlbumsView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.tableView registerClass:[ImagePickerPhotoLibraryAlbumsViewCell class]
         forCellReuseIdentifier:NSStringFromClass([ImagePickerPhotoLibraryAlbumsViewCell class])];
    [self addSubview:self.tableView];
    
    [[PHPhotoLibrary sharedPhotoLibrary] registerChangeObserver:self];
    [self reloadAlbums];
    self.authorizationStatus = [PHPhotoLibrary authorizationStatus];
}

- (void)dealloc {
    
    [[PHPhotoLibrary sharedPhotoLibrary] unregisterChangeObserver:self];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.tableView.frame = self.bounds;
}

- (void)setInsets:(UIEdgeInsets)insets {
    _insets = insets;
    self.tableView.contentInset = insets;
    self.tableView.contentOffset = (CGPoint){
        .y = -insets.top,
    };
}

#pragma mark - Albums

- (void)reloadAlbums {
    
    PHFetchResult *cameraRollFetch = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum
                                                                              subtype:PHAssetCollectionSubtypeSmartAlbumUserLibrary options:nil];
    
    PHFetchResult *otherFetchAlbums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeAlbum
                                                                               subtype:PHAssetCollectionSubtypeAny options:nil];
    
    PHFetchOptions *onlyImagesOptions = [PHFetchOptions new];
    onlyImagesOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %i", PHAssetMediaTypeImage];
    
    NSMutableArray *albums = [[NSMutableArray alloc] init];
    for (PHAssetCollection *collection in cameraRollFetch) {
        PHFetchResult *photosResult = [PHAsset fetchAssetsInAssetCollection:collection options:onlyImagesOptions];
        if (photosResult.count > 0) {
            [albums addObject:collection];
        }
    }
    for (PHAssetCollection *collection in otherFetchAlbums) {
        PHFetchResult *photosResult = [PHAsset fetchAssetsInAssetCollection:collection options:onlyImagesOptions];
        if (photosResult.count > 0) {
            [albums addObject:collection];
        }
    }
    self.albums = albums.copy;
}

#pragma mark - Protocols

#pragma mark UITableViewDelegate/UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 57.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.albums.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    PHAssetCollection *assetCollection = self.albums[indexPath.row];
    
    NSString *identifier = NSStringFromClass([ImagePickerPhotoLibraryAlbumsViewCell class]);
    ImagePickerPhotoLibraryAlbumsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = assetCollection.localizedTitle;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu photos", (unsigned long)[assetCollection assetCount]];
    
    UIImage *cachedThumbnail = assetCollection.cachedThumbnail;
    if (cachedThumbnail) {
        cell.imageView.image = cachedThumbnail;
    } else {
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
        fetchOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %i", PHAssetMediaTypeImage];
        PHFetchResult *photosResult = [PHAsset fetchAssetsInAssetCollection:assetCollection options:fetchOptions];
        
        if (cell.tag != indexPath.row) {
            cell.imageView.image = nil;
            cell.tag = indexPath.row;
            [photosResult.lastObject rsb_imagepicker_requestImageWithSize:CGSizeMake(80.0f, 80.0f)
                                                              contentMode:PHImageContentModeAspectFill
                                                            resultHandler:^(UIImage *image) {
                                                                assetCollection.cachedThumbnail = image;
                                                                if (cell.tag == indexPath.row) {
                                                                    [cell.imageView setImage:image];
                                                                }
                                                            }];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoLibraryAlbumsView:didSelectAssetCollection:)]) {
        [self.delegate imagePickerPhotoLibraryAlbumsView:self didSelectAssetCollection:self.albums[indexPath.row]];
    }
}

#pragma mark - PHPhotoLibraryChangeObserver

- (void)photoLibraryDidChange:(PHChange *)changeInstance {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.authorizationStatus = [PHPhotoLibrary authorizationStatus];
        [self reloadAlbums];
        [self.tableView reloadData];
    });
}

@end
