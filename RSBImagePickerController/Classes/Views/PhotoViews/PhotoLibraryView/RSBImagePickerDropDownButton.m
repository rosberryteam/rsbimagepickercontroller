//
//  TRDDropDownButton.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 08/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "RSBImagePickerDropDownButton.h"

#import "UIImage+RSBImagePickerBundle.h"

@interface RSBImagePickerDropDownButton ()

@property (nonatomic) UIView *containerView;
@property (nonatomic) UILabel *label;
@property (nonatomic) UIImageView *imageView;

@end

@implementation RSBImagePickerDropDownButton

- (instancetype)init {
    
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.animationDuration = 0.15f;
    
    self.containerView = [[UIView alloc] init];
    [self addSubview:self.containerView];
    
    self.label = [[UILabel alloc] init];
    self.label.textAlignment = NSTextAlignmentRight;
    [self.containerView addSubview:self.label];
    
    self.imageView = [[UIImageView alloc] initWithImage:[UIImage rsb_imagepicker_bundledImage:@"RSBImagePickerIconDropDown"]];
    [self.containerView addSubview:self.imageView];
    
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)]];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.label sizeToFit];
    [self.imageView sizeToFit];
    
    self.containerView.bounds = (CGRect){
        .size.width = self.label.bounds.size.width + self.imageView.bounds.size.width + 4.0,
        .size.height = self.bounds.size.height,
    };
    self.containerView.center = (CGPoint){
        .x = self.bounds.size.width / 2.0,
        .y = self.bounds.size.height / 2.0,
    };
    
    self.label.frame = (CGRect){
        .size.width = self.label.bounds.size.width,
        .size.height = self.containerView.bounds.size.height,
    };
    
    self.imageView.center = (CGPoint){
        .x = self.containerView.bounds.size.width - self.imageView.bounds.size.width / 2.0,
        .y = self.containerView.bounds.size.height / 2.0,
    };
}

- (CGSize)sizeThatFits:(CGSize)size{
    [self.label sizeToFit];
    [self.imageView sizeToFit];
    
    return (CGSize){
        .width = self.label.bounds.size.width + self.imageView.bounds.size.width + 4.0,
        .height = MAX(self.label.bounds.size.height, self.imageView.bounds.size.height),
    };
}

#pragma mark - Setters/Getters

- (void)setSelected:(BOOL)selected {
    
    _selected = selected;

    CGFloat fromAngle = 0.0f;
    CGFloat toAngle = -M_PI;
    if (self.selected) {
        [self rotateViewAnimated:self.imageView withDuration:self.animationDuration fromAngle:fromAngle toAngle:toAngle];
    }
    else {
        [self rotateViewAnimated:self.imageView withDuration:self.animationDuration fromAngle:toAngle toAngle:fromAngle];
    }
}

#pragma mark - Actions

- (void)onTap:(id)sender {
    
    self.selected = !self.selected;
    if ([self.delegate respondsToSelector:@selector(dropDownButtonPressed:)]) {
        [self.delegate dropDownButtonPressed:self];
    }
}

#pragma mark - Animation

- (void)rotateViewAnimated:(UIView *)view
              withDuration:(CFTimeInterval)duration
                 fromAngle:(CGFloat)fromValue
                   toAngle:(CGFloat)toValue {
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.fromValue = @(fromValue);
    animation.toValue = @(toValue);
    animation.duration = duration;
    animation.removedOnCompletion = YES;
    [view.layer addAnimation:animation forKey:@"rotation"];
    view.transform = CGAffineTransformMakeRotation(toValue);
}

@end
