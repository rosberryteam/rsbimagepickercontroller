//
//  TRDDropDownButton.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 08/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RSBImagePickerDropDownButton;

@protocol RSBImagePickerDropDownButtonDelegate <NSObject>

@optional
- (void)dropDownButtonPressed:(RSBImagePickerDropDownButton *)button;

@end

@interface RSBImagePickerDropDownButton : UIView

@property (nonatomic, weak) id<RSBImagePickerDropDownButtonDelegate> delegate;
@property (nonatomic) NSTimeInterval animationDuration;
@property (nonatomic) BOOL selected;
@property (nonatomic, readonly) UILabel *label;

@end
