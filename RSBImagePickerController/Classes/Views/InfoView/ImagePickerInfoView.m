//
//  ImagePickerInfoView.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerInfoView.h"

#import "UIImage+RSBImagePickerBundle.h"

@interface ImagePickerInfoView ()

@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UIButton *subtitleButton;

@end

@implementation ImagePickerInfoView

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.imageView = [[UIImageView alloc] init];
        self.imageView.image = [UIImage rsb_imagepicker_bundledImage:@"RSBImagePickerIconGalleryLocked"];
        [self addSubview:self.imageView];
        
        self.titleLabel = [[UILabel alloc] init];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.font = [UIFont systemFontOfSize:14.0];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.titleLabel];
        
        self.subtitleButton = [[UIButton alloc] init];
        [self.subtitleButton addTarget:self action:@selector(subtitleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.subtitleButton];
        
        NSString *colorText = NSLocalizedString(@"Settings", nil);
        NSString *text = [NSString stringWithFormat:NSLocalizedString(@"You can enable access\nin %@", nil), colorText];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
        [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:15] range:NSMakeRange(0, text.length)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, text.length)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:[text rangeOfString:colorText]];
        [self.subtitleButton setAttributedTitle:attributedString forState:UIControlStateNormal];
        self.subtitleButton.titleLabel.numberOfLines = 0;
        self.subtitleButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.imageView sizeToFit];
    self.imageView.center = (CGPoint){
        .x = self.bounds.size.width / 2.0,
        .y = self.bounds.size.height / 2.0 - 36.0 + self.verticalContentOffset,
    };
    
    CGSize titleLabelSize = [self.titleLabel sizeThatFits:(CGSize){.width = self.bounds.size.width - 16.0}];
    self.titleLabel.frame = (CGRect){
        .origin.x = (self.bounds.size.width - titleLabelSize.width) / 2.0,
        .origin.y = CGRectGetMaxY(self.imageView.frame) + 16.0,
        .size = titleLabelSize,
    };
    
    [self.subtitleButton sizeToFit];
    self.subtitleButton.center = (CGPoint){
        .x = self.bounds.size.width / 2.0,
        .y = CGRectGetMaxY(self.titleLabel.frame) + self.subtitleButton.bounds.size.height / 2.0 + 16.0,
    };
}

#pragma mark - Setters/Getters

- (void)setVerticalContentOffset:(CGFloat)verticalContentOffset {
    
    _verticalContentOffset = verticalContentOffset;
    [self setNeedsLayout];
}

- (void)setTitle:(NSString *)title {
    
    _title = title;
    self.titleLabel.text = _title;
}

#pragma mark - Actions

- (IBAction)subtitleButtonPressed:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}

@end
