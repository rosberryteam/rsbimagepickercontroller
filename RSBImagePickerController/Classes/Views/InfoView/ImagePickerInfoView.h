//
//  ImagePickerInfoView.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 13/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerInfoView : UIView

@property (nonatomic) CGFloat verticalContentOffset;
@property (nonatomic) NSString *title;

@end
