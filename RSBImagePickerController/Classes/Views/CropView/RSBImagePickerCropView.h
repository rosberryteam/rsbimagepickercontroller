//
//  CropView.h
//  Collaborate
//
//  Created by EvgenyMikhaylov on 2/25/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RSBImagePickerCropView;

@protocol RSBImagePickerCropViewDelegate <NSObject>

@optional
- (void)cropView:(RSBImagePickerCropView *)view didChangeVisibleRect:(CGRect)rect;

@end

@interface RSBImagePickerCropView : UIView

@property (weak, nonatomic) id<RSBImagePickerCropViewDelegate> delegate;
@property (nonatomic) CGSize contentSize;
@property (nonatomic) UIImage *image;
@property (nonatomic, readonly) UIGestureRecognizer *scrollPanGestureRecognizer;
@property (nonatomic, readonly) CGRect cropRect;
@property (nonatomic, readonly) BOOL scrollDecelerated;

@end
