//
//  ZoomView.m
//  Collaborate
//
//  Created by EvgenyMikhaylov on 2/25/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "RSBImagePickerCropView.h"

@interface RSBImagePickerCropView () <UIScrollViewDelegate>

@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UIImageView *contentView;

@property (nonatomic) CGFloat defaultZoomScale;

@end

@implementation RSBImagePickerCropView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    _scrollDecelerated = YES;

    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.alwaysBounceHorizontal = YES;
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.delegate = self;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:self.scrollView];

    self.contentView = [[UIImageView alloc] init];
    self.contentView.backgroundColor = [[UIColor orangeColor] colorWithAlphaComponent:0.5f];
    self.contentView.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:self.contentView];

    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapAction:)];
    doubleTap.numberOfTapsRequired = 2;
    
    [self.scrollView addGestureRecognizer:doubleTap];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (CGRectIsEmpty(self.bounds))
        return;

    self.scrollView.frame = self.bounds;
    self.scrollView.contentSize = self.contentSize;
    
    if (CGSizeEqualToSize(self.contentSize, CGSizeZero))
        return;

    CGFloat minimumZoomScale;
    
    if (self.contentSize.height >= self.contentSize.width) {
        minimumZoomScale = CGRectGetHeight(self.scrollView.frame) / self.contentSize.width;
    } else {
        minimumZoomScale = CGRectGetWidth(self.scrollView.frame) / self.contentSize.height;
    }
    
    self.scrollView.minimumZoomScale = minimumZoomScale;
    self.scrollView.maximumZoomScale = minimumZoomScale*10.0f;
    [self.scrollView setZoomScale:minimumZoomScale];
    [self resetContentOffset];
}

#pragma mark - UIGestureRecognizer

- (void)doubleTapAction:(UITapGestureRecognizer*)recognizer
{
    [self setNeedsLayout];
}

- (void)resetContentOffset
{
    CGFloat offsetX = 0.0f;
    if (CGRectGetWidth(self.contentView.frame)>CGRectGetWidth(self.scrollView.frame)){
        offsetX = 0.5f*(CGRectGetWidth(self.contentView.frame)-CGRectGetWidth(self.scrollView.frame));
    }
    CGFloat offsetY = 0.0f;
    if (CGRectGetHeight(self.contentView.frame)>CGRectGetHeight(self.scrollView.frame)){
        offsetY = 0.5f*(CGRectGetHeight(self.contentView.frame)-CGRectGetHeight(self.scrollView.frame));
    }
    [self.scrollView setContentOffset:CGPointMake(offsetX, offsetY)];
}

- (void)centerScrollViewContent
{
    CGRect contentViewFrame = self.contentView.frame;
    if (CGRectGetWidth(contentViewFrame)<CGRectGetWidth(self.scrollView.frame)) {
        contentViewFrame.origin.x = (CGRectGetWidth(self.scrollView.frame)-CGRectGetWidth(contentViewFrame))/2;
    }else {
        contentViewFrame.origin.x = 0;
    }
    if (CGRectGetHeight(contentViewFrame)<CGRectGetHeight(self.scrollView.frame)) {
        contentViewFrame.origin.y = (CGRectGetHeight(self.scrollView.frame)-CGRectGetHeight(contentViewFrame))/2;
    }else {
        contentViewFrame.origin.y = 0;
    }
    
    self.contentView.frame = contentViewFrame;
}

- (void)scrollViewVisibleRectChanged
{
    if ([self.delegate respondsToSelector:@selector(cropView:didChangeVisibleRect:)]){
        CGRect visibleRect = [self.scrollView convertRect:self.scrollView.bounds toView:self.contentView];
        [self.delegate cropView:self didChangeVisibleRect:visibleRect];
    }
}

#pragma mark -
#pragma mark Setters/Getters

- (void)setContentSize:(CGSize)contentSize
{
    _contentSize = contentSize;
    self.contentView.transform = CGAffineTransformIdentity;
    self.contentView.frame = (CGRect){
        .origin = CGPointZero,
        .size = _contentSize,
    };
    [self setNeedsLayout];
}

- (void)setImage:(UIImage *)image
{
    _image = image;
    self.contentView.image = image;
    self.contentSize = _image.size;
    
    [self setNeedsLayout];
}

- (UIGestureRecognizer *)scrollPanGestureRecognizer
{
    return self.scrollView.panGestureRecognizer;
}

- (CGRect)cropRect
{
    CGRect visibleRect = [self.scrollView convertRect:self.scrollView.bounds toView:self.contentView];
    CGRect cropRect = (CGRect) {
        .origin.x = CGRectGetMinX(visibleRect) / self.image.size.width,
        .origin.y = CGRectGetMinY(visibleRect) / self.image.size.height,
        .size.width = CGRectGetWidth(visibleRect) / self.image.size.width,
        .size.height = CGRectGetHeight(visibleRect) / self.image.size.height,
    };
    if (CGRectGetMinX(cropRect) < 0.0f) {
        cropRect.origin.x = 0.0f;
    }
    if (CGRectGetMinY(cropRect) < 0.0f) {
        cropRect.origin.y = 0.0f;
    }
    if (CGRectGetWidth(cropRect) > 1.0f) {
        cropRect.size.width = 1.0f;
    }
    if (CGRectGetHeight(cropRect) > 1.0f) {
        cropRect.size.height = 1.0f;
    }
    return cropRect;
}

#pragma mark -
#pragma mark UIScrollViewDelegate

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    _scrollDecelerated = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _scrollDecelerated = YES;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    [self centerScrollViewContent];
    [self scrollViewVisibleRectChanged];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.contentView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self scrollViewVisibleRectChanged];
}

@end
