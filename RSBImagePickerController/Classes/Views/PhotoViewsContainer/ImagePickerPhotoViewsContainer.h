//
//  ImagePickerPhotoViewsContainer.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 09/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImagePickerPhotoViewAttributes.h"

@class ImagePickerPhotoViewsContainer;

@protocol ImagePickerPhotoViewsContainerDelegate <NSObject>

@optional
- (void)imagePickerPhotoViewsContainerPhotoViewDidSelect:(ImagePickerPhotoViewsContainer *)view
                                                 atIndex:(NSUInteger)index;

@end

@interface ImagePickerPhotoViewsContainer : UIView

@property (nonatomic, weak) id<ImagePickerPhotoViewsContainerDelegate> delegate;
@property (nonatomic, copy) NSArray<UIView<ImagePickerPhotoViewAttributes> *> *views;
@property (nonatomic) NSUInteger selectedIndex;

@end
