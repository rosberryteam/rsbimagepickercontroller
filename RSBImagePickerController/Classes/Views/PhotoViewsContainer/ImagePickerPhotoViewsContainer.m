//
//  ImagePickerPhotoViewsContainer.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 09/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerPhotoViewsContainer.h"

@interface ImagePickerPhotoViewsContainer ()

@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) CGPoint initialScrollViewContentOffset;

@end

@implementation ImagePickerPhotoViewsContainer

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.scrollView = [[UIScrollView alloc] init];
        self.scrollView.showsHorizontalScrollIndicator = NO;
        self.scrollView.scrollEnabled = NO;
        UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureAction:)];
        [self.scrollView addGestureRecognizer:panGestureRecognizer];

        [self addSubview:self.scrollView];
    }
    return self;
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.scrollView.frame = self.bounds;
    self.scrollView.contentSize = CGSizeMake(self.views.count * CGRectGetWidth(self.scrollView.bounds), CGRectGetHeight(self.scrollView.bounds));
    
    NSUInteger viewIndex = 0;
    for (UIView *view in self.views) {
        view.frame = (CGRect){
            .origin.x = self.scrollView.bounds.size.width * viewIndex++,
            .size = self.scrollView.bounds.size,
        };
    }
}

#pragma mark - Setters/Getters

- (void)setViews:(NSArray<UIView<ImagePickerPhotoViewAttributes> *> *)views {
    
    for (UIView *view in self.views) {
        [view removeFromSuperview];
    }
    
    _views = [views copy];
    
    for (UIView *view in self.views) {
        [self.scrollView addSubview:view];
    }
    
    [self setNeedsLayout];
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex {
    
    _selectedIndex = selectedIndex;
    
    CGFloat offset = _selectedIndex * CGRectGetWidth(self.scrollView.frame);
    self.scrollView.contentOffset = CGPointMake(offset, self.scrollView.contentOffset.y);
}

#pragma mark - Helpers

- (void)updateSelectedIndex {
    
    NSUInteger selectedIndex = self.scrollView.contentOffset.x / CGRectGetWidth(self.scrollView.frame);
    if (_selectedIndex == selectedIndex) {
        return;
    }
    _selectedIndex = selectedIndex;
    if ([self.delegate respondsToSelector:@selector(imagePickerPhotoViewsContainerPhotoViewDidSelect:atIndex:)]) {
        [self.delegate imagePickerPhotoViewsContainerPhotoViewDidSelect:self atIndex:_selectedIndex];
    }
}

#pragma mark - Actions

- (void)panGestureAction:(UIPanGestureRecognizer *)gestureRecognizer {
    
    CGPoint translation = [gestureRecognizer translationInView:self];
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.initialScrollViewContentOffset = self.scrollView.contentOffset;
    }
    CGFloat scrollViewWidth = CGRectGetWidth(self.scrollView.frame);
    CGFloat newOffset = self.initialScrollViewContentOffset.x -  translation.x;
    CGFloat minOffset = 0.0f;
    CGFloat maxOffset = self.scrollView.contentSize.width - scrollViewWidth;
    if (newOffset <= minOffset) {
        self.scrollView.contentOffset = CGPointMake(minOffset, self.scrollView.contentOffset.y);
    }
    else if (newOffset >= maxOffset) {
        self.scrollView.contentOffset = CGPointMake(maxOffset, self.scrollView.contentOffset.y);
    }
    else {
        if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
            CGPoint velocity = [gestureRecognizer velocityInView:self];
            NSUInteger index = (newOffset + scrollViewWidth / 2) / scrollViewWidth;
            if (fabs(velocity.x) > 100) {
                NSInteger sign = - velocity.x / fabs(velocity.x);
                index = (newOffset + sign * scrollViewWidth) / scrollViewWidth;
            }
            [UIView animateWithDuration:0.2f delay:0.0f options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.scrollView.contentOffset = CGPointMake(scrollViewWidth * index, self.scrollView.contentOffset.y);
            } completion:^(BOOL finished) {
                [self updateSelectedIndex];
            }];
        }
        else {
            self.scrollView.contentOffset = CGPointMake(newOffset, self.scrollView.contentOffset.y);
        }
    }
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        [self updateSelectedIndex];
    }
}

@end
