//
//  ImagePickerTabBar.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 09/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "ImagePickerTabBar.h"
#import "ImagePickerTabBarItem.h"

@implementation ImagePickerTabBar

#pragma mark - Setters/Getters

- (void)setItems:(NSArray<ImagePickerTabBarItem *> *)items {
    
    _items = [items copy];
    
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    [_items enumerateObjectsUsingBlock:^(ImagePickerTabBarItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {

        UIButton *button = [[UIButton alloc] init];
        button.titleLabel.font = obj.font;
        [button setTitle:obj.title forState:UIControlStateNormal];
        [button setTitleColor:obj.normalTitleColor forState:UIControlStateNormal];
        [button setTitleColor:obj.selectedTitleColor forState:UIControlStateHighlighted];
        [button setTitleColor:obj.selectedTitleColor forState:UIControlStateSelected];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = idx;
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:button];
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                         attribute:NSLayoutAttributeTop
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeTop
                                                        multiplier:1.0f
                                                          constant:0.0f]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                         attribute:NSLayoutAttributeBottom
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:self
                                                         attribute:NSLayoutAttributeBottom
                                                        multiplier:1.0f
                                                          constant:0.0f]];
        
        if (idx == 0) {
            [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:self
                                                             attribute:NSLayoutAttributeLeft
                                                            multiplier:1.0f
                                                              constant:0.0f]];
        }
        else {
            UIButton *previousButton = [self buttonAtIndex:idx-1];
            [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                             attribute:NSLayoutAttributeLeft
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:previousButton
                                                             attribute:NSLayoutAttributeRight
                                                            multiplier:1.0f
                                                              constant:0.0f]];
            [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:previousButton
                                                             attribute:NSLayoutAttributeWidth
                                                            multiplier:1.0f
                                                              constant:0.0f]];
            if (idx == (self.items.count - 1)) {
                [self addConstraint:[NSLayoutConstraint constraintWithItem:button
                                                                 attribute:NSLayoutAttributeRight
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self
                                                                 attribute:NSLayoutAttributeRight
                                                                multiplier:1.0f
                                                                  constant:0.0f]];
            }
        }
    }];
    self.selectedIndex = 0;
}

- (void)setSelectedIndex:(NSUInteger)selectedIndex {
    
    _selectedIndex = selectedIndex;
    [self.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[UIButton class]]) {
            UIButton *button = obj;
            button.selected = (idx == selectedIndex);
        }
    }];
}

#pragma mark - Buttons

- (UIButton *)buttonAtIndex:(NSUInteger)index {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tag == %d", index];
    return [[self.subviews filteredArrayUsingPredicate:predicate] firstObject];
}

#pragma mark - Actions

- (void)buttonPressed:(UIButton *)button {
    
    self.selectedIndex = button.tag;
    if ([self.delegate respondsToSelector:@selector(imagePickerTabBar:itemAtIndexSelected:)]) {
        [self.delegate imagePickerTabBar:self itemAtIndexSelected:self.selectedIndex];
    }
}

@end
