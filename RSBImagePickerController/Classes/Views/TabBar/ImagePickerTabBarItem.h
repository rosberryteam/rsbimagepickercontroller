//
//  ImagePickerTabBarItem.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 09/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerTabBarItem : NSObject

@property (nonatomic) NSString *title;
@property (nonatomic) UIFont *font;
@property (nonatomic) UIColor *normalTitleColor;
@property (nonatomic) UIColor *selectedTitleColor;

@end
