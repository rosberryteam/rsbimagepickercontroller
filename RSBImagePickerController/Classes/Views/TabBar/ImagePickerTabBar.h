//
//  ImagePickerTabBar.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 09/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImagePickerTabBar;

@protocol ImagePickerTabBarDelegate <NSObject>

@optional
- (void)imagePickerTabBar:(ImagePickerTabBar *)tabBar itemAtIndexSelected:(NSUInteger)index;

@end

@class ImagePickerTabBarItem;

@interface ImagePickerTabBar : UIView

@property (nonatomic, weak) id<ImagePickerTabBarDelegate> delegate;
@property (nonatomic, copy) NSArray<ImagePickerTabBarItem *> *items;
@property (nonatomic) NSUInteger selectedIndex;

@end
