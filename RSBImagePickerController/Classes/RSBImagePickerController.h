//
//  RSBImagePickerController.h
//  Pods
//
//  Created by Anton K on 6/6/16.
//
//

#import <UIKit/UIKit.h>

#import "RSBImagePickerViewControllerDelegate.h"

@interface RSBImagePickerController : UINavigationController

@property (nonatomic, weak) id<RSBImagePickerViewControllerDelegate, UINavigationControllerDelegate> delegate;

@property (nonatomic, assign) CGSize fitSize;
@property (nonatomic, assign) BOOL shouldShowMask;

@end
