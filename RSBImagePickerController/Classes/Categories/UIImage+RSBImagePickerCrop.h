//
//  UIImage+Crop.h
//  CircleCam
//
//  Created by Paul Bar on 2/20/13.
//  Copyright (c) 2013 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (RSBImagePickerCrop)

- (UIImage *)rsb_imagepicker_fixOrientation;
- (UIImage *)rsb_imagepicker_squareImage;
- (UIImage *)rsb_imagepicker_cropWithAbsoluteRect:(CGRect)rect;
- (UIImage *)rsb_imagepicker_crop:(CGRect)rect;
- (CGRect)rsb_imagepicker_cropRectInContainerWithSize:(CGSize)containerSize;
+ (CGRect)rsb_imagepicker_cropRectForImageWithSize:(CGSize)imageSize inContainerWithSize:(CGSize)containerSize;
+ (CGRect)rsb_imagepicker_cropRectForImageWithSize:(CGSize)imageSize scale:(CGFloat)scale inContainerWithSize:(CGSize)containerSize;

@end
