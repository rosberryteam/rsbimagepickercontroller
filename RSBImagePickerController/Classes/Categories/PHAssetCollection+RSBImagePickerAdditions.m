//
//  PHAssetCollection+RSBImagePickerAdditions.m
//  Pods
//
//  Created by Anton K on 6/10/16.
//
//

#import "PHAssetCollection+RSBImagePickerAdditions.h"
#import <objc/runtime.h>

@implementation PHAssetCollection (RSBImagePickerAdditions)

- (void)setCachedThumbnail:(UIImage *)thumbnail {
    objc_setAssociatedObject(self, @selector(setCachedThumbnail:), thumbnail, OBJC_ASSOCIATION_RETAIN);
}

- (UIImage *)cachedThumbnail {
    return objc_getAssociatedObject(self, @selector(setCachedThumbnail:));
}

- (NSUInteger)assetCount {
    NSUInteger estimatedAssetCount = self.estimatedAssetCount;
    if (estimatedAssetCount == NSNotFound) {
        PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
        fetchOptions.predicate = [NSPredicate predicateWithFormat:@"mediaType = %i", PHAssetMediaTypeImage];
        PHFetchResult *photosResult = [PHAsset fetchAssetsInAssetCollection:self options:fetchOptions];
        return [photosResult count];
    }
    
    return estimatedAssetCount;
}

@end
