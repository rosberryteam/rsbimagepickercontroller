//
//  PHAsset+Additions.h
//  Cinepic
//
//  Created by EvgenyMikhaylov on 6/11/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import <Photos/Photos.h>

@interface PHAsset (RSBImagePickerAdditions)

- (void)rsb_imagepicker_requestImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode resultHandler:(void(^)(UIImage*))resultHandler;

+ (PHAsset *)rsb_imagepicker_lastAssetWithType:(PHAssetMediaType)type;
+ (void)rsb_imagepicker_requestLastImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode resultHandler:(void(^)(UIImage*))resultHandler;

@end
