//
//  UIImage+RSBImagePickerBundle.h
//  Pods
//
//  Created by Anton K on 6/6/16.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (RSBImagePickerBundle)

+ (UIImage *)rsb_imagepicker_bundledImage:(NSString *)imageName;

@end
