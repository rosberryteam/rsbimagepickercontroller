//
//  UIImage+Resize.m
//  Peek
//
//  Created by EvgenyMikhaylov on 10/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import "UIImage+RSBImagePickerResize.h"

@implementation UIImage (RSBImagePickerResize)

- (UIImage *)rsb_imagepicker_resizeImageWithMaxDimension:(CGFloat)maxDimension
{
    CGFloat aspectRatio = self.size.width/self.size.height;
    CGSize imageSize = (aspectRatio>1) ? CGSizeMake(maxDimension, maxDimension/aspectRatio) : CGSizeMake(maxDimension*aspectRatio, maxDimension);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 1.0);
    [self drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}

- (UIImage *)rsb_imagepicker_resizeImageWithMinDimension:(CGFloat)minDimension
{
    CGFloat aspectRatio = self.size.width/self.size.height;
    CGSize imageSize = (aspectRatio>1) ? CGSizeMake(minDimension*aspectRatio, minDimension) : CGSizeMake(minDimension, minDimension/aspectRatio);
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 1.0);
    [self drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}


@end
