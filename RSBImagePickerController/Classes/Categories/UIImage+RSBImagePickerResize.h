//
//  UIImage+Resize.h
//  Peek
//
//  Created by EvgenyMikhaylov on 10/30/14.
//  Copyright (c) 2014 Rosberry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (RSBImagePickerResize)

- (UIImage *)rsb_imagepicker_resizeImageWithMaxDimension:(CGFloat)maxDimension;
- (UIImage *)rsb_imagepicker_resizeImageWithMinDimension:(CGFloat)minDimension;

@end
