//
//  PHAssetCollection+RSBImagePickerAdditions.h
//  Pods
//
//  Created by Anton K on 6/10/16.
//
//

#import <Photos/Photos.h>

@interface PHAssetCollection (RSBImagePickerAdditions)

@property (nonatomic, strong) UIImage *cachedThumbnail;

- (NSUInteger) assetCount;

@end
