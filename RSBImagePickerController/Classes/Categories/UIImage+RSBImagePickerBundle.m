//
//  UIImage+RSBImagePickerBundle.m
//  Pods
//
//  Created by Anton K on 6/6/16.
//
//

#import "UIImage+RSBImagePickerBundle.h"

#import "RSBImagePickerController.h"

@implementation UIImage (RSBImagePickerBundle)

+ (UIImage *)rsb_imagepicker_bundledImage:(NSString *)imageName {
    UIImage *image = [UIImage imageNamed:imageName];
    if (image) {
        return image;
    }
    
    NSBundle *resourceBundle = [NSBundle bundleWithURL:[[NSBundle bundleForClass:[RSBImagePickerController class]] URLForResource:@"RSBImagePickerController" withExtension:@"bundle"]];
    return [UIImage imageNamed:imageName inBundle:resourceBundle compatibleWithTraitCollection:nil];
}

@end
