//
//  PHAsset+Additions.m
//  Cinepic
//
//  Created by EvgenyMikhaylov on 6/11/15.
//  Copyright (c) 2015 Rosberry. All rights reserved.
//

#import "PHAsset+RSBImagePickerAdditions.h"

@implementation PHAsset (RSBImagePickerAdditions)

+ (PHAsset *)rsb_imagepicker_lastAssetWithType:(PHAssetMediaType)type
{
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:type options:fetchOptions];
    return fetchResult.lastObject;
}

- (void)rsb_imagepicker_requestImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode resultHandler:(void(^)(UIImage*))resultHandler
{
    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.synchronous = YES;
    options.networkAccessAllowed = YES;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [[PHImageManager defaultManager] requestImageForAsset:self
                                                   targetSize:size
                                                  contentMode:contentMode
                                                      options:options
                                                resultHandler:^(UIImage *result, NSDictionary *info) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if (resultHandler){
                                                            resultHandler(result);
                                                        }
                                                    });
                                                }];
    });
}

+ (void)rsb_imagepicker_requestLastImageWithSize:(CGSize)size contentMode:(PHImageContentMode)contentMode resultHandler:(void(^)(UIImage*))resultHandler
{
    PHAsset *asset = [PHAsset rsb_imagepicker_lastAssetWithType:PHAssetMediaTypeImage];
    if (asset){
        PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
        options.synchronous = YES;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [[PHImageManager defaultManager] requestImageForAsset:asset
                                                       targetSize:size
                                                      contentMode:contentMode
                                                          options:options
                                                    resultHandler:^(UIImage *result, NSDictionary *info) {
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            if (resultHandler){
                                                                resultHandler(result);
                                                            }
                                                        });
                                                    }];
        });
    }
    else{
        if (resultHandler){
            resultHandler(nil);
        }
    }
}

@end
