//
//  RSBImagePickerController.m
//  Pods
//
//  Created by Anton K on 6/6/16.
//
//

#import "RSBImagePickerController.h"

#import "RSBImagePickerViewController.h"

@interface RSBImagePickerController ()

@property (nonatomic, strong) RSBImagePickerViewController *controller;

@end

@implementation RSBImagePickerController

@dynamic delegate;

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setViewControllers:@[[self viewController]]];
    }
    return self;
}

- (id)init {
    if (self = [super initWithRootViewController:[self viewController]]) {
        
    }
    return self;
}

- (RSBImagePickerViewController *)viewController {
    if (!_controller) {
        _controller = [[RSBImagePickerViewController alloc] init];
    }
    
    return _controller;
}

- (void)setDelegate:(id<RSBImagePickerViewControllerDelegate, UINavigationControllerDelegate>)delegate {
    [super setDelegate:delegate];
    [self viewController].delegate = delegate;
}

- (void)setFitSize:(CGSize)fitSize {
    [self viewController].imageSize = fitSize;
}

- (CGSize)fitSize {
    return [self viewController].imageSize;
}

- (void)setShouldShowMask:(BOOL)shouldShowMask {
    [self viewController].showMask = shouldShowMask;
}

- (BOOL)shouldShowMask {
    return [self viewController].showMask;
}

@end
