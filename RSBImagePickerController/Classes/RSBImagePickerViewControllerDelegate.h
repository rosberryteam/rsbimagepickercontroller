//
//  RSBImagePickerDelegate.h
//  Pods
//
//  Created by Anton K on 6/6/16.
//
//

#import <Foundation/Foundation.h>

@class RSBImagePickerViewController;

@protocol RSBImagePickerViewControllerDelegate <NSObject>

@optional
- (void)imagePickerController:(RSBImagePickerViewController *)imagePickerController didSelectImage:(UIImage *)image;

@end