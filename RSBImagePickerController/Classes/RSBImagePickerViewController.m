//
//  ImagePickerController.m
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 08/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import "RSBImagePickerViewController.h"
#import "ImagePickerPhotoLibraryAlbumsView.h"
#import "ImagePickerPhotoViewsContainer.h"
#import "ImagePickerPhotoLibraryView.h"
#import "ImagePickerPhotoCameraView.h"
#import "ImagePickerTabBar.h"

#import "ImagePickerTabBarItem.h"

#import "UIImage+RSBImagePickerCrop.h"
#import "UIImage+RSBImagePickerResize.h"
#import "UIImage+RSBImagePickerBundle.h"

#import "RSBImagePickerViewControllerDelegate.h"

@interface RSBImagePickerViewController ()
<
ImagePickerPhotoViewsContainerDelegate,
ImagePickerPhotoLibraryViewDelegate,
ImagePickerPhotoCameraViewDelegate,
ImagePickerPhotoLibraryAlbumsViewDelegate,
ImagePickerTabBarDelegate
>

@property (nonatomic) ImagePickerPhotoViewsContainer *photoViewsContainer;
@property (nonatomic) ImagePickerPhotoLibraryView *photoLibraryView;
@property (nonatomic) ImagePickerPhotoCameraView *photoCameraView;
@property (nonatomic) NSArray<UIView<ImagePickerPhotoViewAttributes> *> *photoViews;
@property (nonatomic) UIView *photoLibraryAlbumsViewContainer;
@property (nonatomic) ImagePickerPhotoLibraryAlbumsView *photoLibraryAlbumsView;
@property (nonatomic) ImagePickerTabBar *tabBar;
@property (nonatomic, readonly) CGFloat tabBarHeight;

@property (nonatomic) UIBarButtonItem *cancelBarButtonItem;
@property (nonatomic) UIBarButtonItem *saveBarButtonItem;

@end

@implementation RSBImagePickerViewController

- (instancetype)init {
    if (self = [super init]) {
        self.showMask = YES;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        self.showMask = YES;
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.cancelBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage rsb_imagepicker_bundledImage:@"RSBImagePickerIconTopCancel"] style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed:)];
    self.navigationItem.leftBarButtonItem = self.cancelBarButtonItem;
    
    self.saveBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage rsb_imagepicker_bundledImage:@"RSBImagePickerIconTopDone"] style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonPressed:)];
    self.navigationItem.rightBarButtonItem = nil;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.photoLibraryView = [[ImagePickerPhotoLibraryView alloc] init];
    self.photoLibraryView.delegate = self;
    self.photoLibraryView.bottomInset = self.tabBarHeight;
    self.photoLibraryView.showMask = self.showMask;
    [self.view addSubview:self.photoLibraryView];
    
    self.photoCameraView = [[ImagePickerPhotoCameraView alloc] init];
    self.photoCameraView.delegate = self;
    self.photoCameraView.bottomInset = self.tabBarHeight;
    self.photoCameraView.showMask = self.showMask;
    [self.view addSubview:self.photoCameraView];
    
    self.photoViews = @[self.photoLibraryView, self.photoCameraView];
    
    NSUInteger photoViewIndex = 0;
    UIView<ImagePickerPhotoViewAttributes> *photoView = self.photoViews[photoViewIndex];
    self.navigationItem.titleView = photoView.titleView;

    self.photoViewsContainer = [[ImagePickerPhotoViewsContainer alloc] init];
    self.photoViewsContainer.delegate = self;
    self.photoViewsContainer.views = self.photoViews;
    self.photoViewsContainer.selectedIndex = photoViewIndex;
    [self.view addSubview:self.photoViewsContainer];
    
    self.tabBar = [[ImagePickerTabBar alloc] init];
    self.tabBar.delegate = self;
    self.tabBar.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.97f];
    NSMutableArray *items = [[NSMutableArray alloc] init];
    [self.photoViews enumerateObjectsUsingBlock:^(UIView<ImagePickerPhotoViewAttributes> * _Nonnull view, NSUInteger idx, BOOL * _Nonnull stop) {
        ImagePickerTabBarItem *item = [[ImagePickerTabBarItem alloc] init];
        item.title = view.title;
        item.font = [UIFont systemFontOfSize:13.0];
        item.normalTitleColor = [[UIColor darkGrayColor] colorWithAlphaComponent:0.3f];
        item.selectedTitleColor = [UIColor darkGrayColor];
        [items addObject:item];
    }];
    self.tabBar.items = items;
    self.tabBar.selectedIndex = photoViewIndex;
    [self.view addSubview:self.tabBar];
    
    self.photoLibraryAlbumsViewContainer = [[UIView alloc] init];
    self.photoLibraryAlbumsViewContainer.hidden = YES;
    [self.view addSubview:self.photoLibraryAlbumsViewContainer];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.photoLibraryView.bottomInset = [self tabBarHeight] + [self.topLayoutGuide length];
    self.photoCameraView.bottomInset = [self tabBarHeight] + [self.topLayoutGuide length];
    
    self.photoLibraryAlbumsViewContainer.frame = (CGRect){
        .size.width = self.view.bounds.size.width,
        .size.height = self.view.bounds.size.height,
    };
    self.photoViewsContainer.frame = self.photoLibraryAlbumsViewContainer.frame;
    
    self.tabBar.frame = (CGRect){
        .origin.y = self.view.bounds.size.height - [self tabBarHeight],
        .size.width = self.view.bounds.size.width,
        .size.height = [self tabBarHeight],
    };
    
    self.photoLibraryAlbumsView.frame = self.photoLibraryAlbumsViewContainer.bounds;
    self.photoLibraryAlbumsView.insets = (UIEdgeInsets){
        .top = [self.topLayoutGuide length],
    };
}

#pragma mark - Setters/Getters

- (void)setShowMask:(BOOL)showMask {
    
    _showMask = showMask;
    self.photoLibraryView.showMask = _showMask;
    self.photoCameraView.showMask = _showMask;
}

- (CGFloat)tabBarHeight {
    
    return 44.0;
}

#pragma mark - Views

- (void)showPhotoLibraryAlbums {
    
    if (self.photoLibraryAlbumsView) {
        return;
    }
    
    self.photoLibraryAlbumsView = [[ImagePickerPhotoLibraryAlbumsView alloc] init];
    self.photoLibraryAlbumsView.delegate = self;
    [self.photoLibraryAlbumsViewContainer addSubview:self.photoLibraryAlbumsView];
    self.photoLibraryAlbumsViewContainer.hidden = NO;
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];

    self.photoLibraryAlbumsView.frame = (CGRect){
        .origin.y = -self.photoLibraryAlbumsViewContainer.bounds.size.height,
        .size = self.photoLibraryAlbumsViewContainer.bounds.size,
    };
    
    [UIView animateWithDuration:0.25 animations:^{
        self.photoLibraryAlbumsView.frame = self.photoLibraryAlbumsViewContainer.bounds;
    }];
}

- (void)hidePhotoLibraryAlbums {
    
    if (!self.photoLibraryAlbumsView) {
        return;
    }
    
    [UIView animateWithDuration:0.25 animations:^{
        self.photoLibraryAlbumsView.frame = (CGRect){
            .origin.y = -self.photoLibraryAlbumsViewContainer.bounds.size.height,
            .size = self.photoLibraryAlbumsViewContainer.bounds.size,
        };
    } completion:^(BOOL finished) {
        self.photoLibraryAlbumsViewContainer.hidden = YES;
        [self.photoLibraryAlbumsView removeFromSuperview];
        self.photoLibraryAlbumsView = nil;
    }];
}

#pragma mark - Actions

- (void)cancelButtonPressed:(UIBarButtonItem *)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveButtonPressed:(UIBarButtonItem *)sender {
    UIView<ImagePickerPhotoViewAttributes> *photoView = self.photoViews[self.photoViewsContainer.selectedIndex];
    __weak typeof (self) weakSelf = self;
    [photoView fetchOriginalImageWithCompletionBlock:^(UIImage *image) {
        if (!image) {
            return;
        }
        [weakSelf processOriginalImage:image withCropRect:photoView.cropRect];
    }];
}

#pragma mark - Original Image

- (void)processOriginalImage:(UIImage *)image withCropRect:(CGRect)cropRect {
    if (CGSizeEqualToSize(self.imageSize, CGSizeZero)) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *croppedImage = [image rsb_imagepicker_cropWithAbsoluteRect:cropRect];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self.delegate respondsToSelector:@selector(imagePickerController:didSelectImage:)]) {
                    [self.delegate imagePickerController:self didSelectImage:croppedImage];
                }
            });
        });
    }
    else {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            UIImage *resizedImage = [image rsb_imagepicker_resizeImageWithMinDimension:self.imageSize.width];
            UIImage *croppedImage = [resizedImage rsb_imagepicker_cropWithAbsoluteRect:cropRect];
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([self.delegate respondsToSelector:@selector(imagePickerController:didSelectImage:)]) {
                    [self.delegate imagePickerController:self didSelectImage:croppedImage];
                }
            });
        });
    }

}

#pragma mark - ImagePickerPhotoViewsContainerDelegate

- (void)imagePickerPhotoViewsContainerPhotoViewDidSelect:(ImagePickerPhotoViewsContainer *)view atIndex:(NSUInteger)index {
    
    UIView<ImagePickerPhotoViewAttributes> *photoView = self.photoViews[index];
    
    self.tabBar.selectedIndex = index;
    self.navigationItem.titleView = photoView.titleView;
    self.navigationItem.rightBarButtonItem = ((photoView.image == nil) || photoView.infoViewEnabled) ? nil : self.saveBarButtonItem;
}

#pragma mark - ImagePickerPhotoViewDelegate

- (void)imagePickerPhotoView:(UIView<ImagePickerPhotoViewAttributes> *)view infoViewEnabled:(BOOL)enabled {
    
    self.navigationItem.rightBarButtonItem = (self.photoViews[self.photoViewsContainer.selectedIndex].infoViewEnabled) ? nil : self.saveBarButtonItem;
}

#pragma mark - ImagePickerPhotoLibraryViewDelegate

- (void)imagePickerPhotoLibraryView:(ImagePickerPhotoLibraryView *)view dropDownButtonDidSelect:(BOOL)selected {

    if (selected) {
        [self showPhotoLibraryAlbums];
    }
    else {
        [self hidePhotoLibraryAlbums];
    }
}

#pragma mark - ImagePickerPhotoCameraViewDelegate

- (void)imagePickerPhotoCameraView:(ImagePickerPhotoCameraView *)view stateChanged:(ImagePickerPhotoCameraState)state {
    
    self.navigationItem.rightBarButtonItem = (view.image == nil) ? nil : self.saveBarButtonItem;
}

#pragma mark - ImagePickerPhotoLibraryAlbumsViewDelegate

- (void)imagePickerPhotoLibraryAlbumsView:(ImagePickerPhotoLibraryAlbumsView *)view
                 didSelectAssetCollection:(PHAssetCollection *)assetCollection {
    
    self.photoLibraryView.assetCollection = assetCollection;
    self.navigationItem.titleView = self.photoLibraryView.titleView;
    [self hidePhotoLibraryAlbums];
}

#pragma mark - ImagePickerTabBarDelegate

- (void)imagePickerTabBar:(ImagePickerTabBar *)tabBar itemAtIndexSelected:(NSUInteger)index {
    
    self.photoViewsContainer.selectedIndex = index;
    self.navigationItem.titleView = self.photoViews[index].titleView;
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
}

@end
