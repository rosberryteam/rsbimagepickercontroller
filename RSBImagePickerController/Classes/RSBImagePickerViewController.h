//
//  ImagePickerController.h
//  ImagePicker
//
//  Created by Evgeny Mikhaylov on 08/02/16.
//  Copyright © 2016 Evgeny Mikhaylov. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RSBImagePickerViewControllerDelegate;

@interface RSBImagePickerViewController : UIViewController

@property (nonatomic, weak) id<RSBImagePickerViewControllerDelegate> delegate;
@property (nonatomic) CGSize imageSize;
@property (nonatomic) BOOL showMask;

@end
